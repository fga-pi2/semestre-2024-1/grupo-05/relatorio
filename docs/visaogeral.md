# **Introdução**


# **Detalhamento do problema**

<div style="text-align: justify">
Desde a segunda metade da década passada, a utilização de painéis solares para geração de energia elétrica em instalações residenciais, industriais e comerciais de pequeno, médio e grande porte vem crescendo de forma significativa. Essa geração já representa, segundo a Empresa de Pesquisa Energética (EPE), 4,4% da geração elétrica do país, e 3,6% da geração elétrica global (EPE, 2023).
</div>
<br>
<div style="text-align: justify">
Dados da Neoenergia (2022) confirmam esta tendência. Foi registrado, de 2021 a 2022, um aumento de 101% no número de pedidos de conexão de energia solar no Distrito Federal. A unidade federativa conta assim com mais de oito mil clientes com usinas solares em funcionamento.
</div>
<br>
<div style="text-align: justify">
O Balanço Energético Nacional (BEN) de 2023 indica que a oferta interna de energia solar, a nível nacional, aumentou em mais de 51% de 2021 a 2022. Este relatório trás ainda a evolução da matriz elétrica do Brasil durante os dez anos anteriores, mostrando que de 2013 a 2023 a participação da produção fotovoltaica na oferta de energia aumentou em 6 vezes. Ou seja, a geração solar de eletricidade representa uma parcela significativa da geração do país, com forte tendência de crescimento, não só no mercado brasileiro, mas também no cenário global.
</div>
<br>
<div style="text-align: justify">
Esta participação crescente da geração com placas solares gera a necessidade de entendimento dos fatores que mais afetam a produtividade de instalações, visando o aumento da produção, e assim o rendimento da instalação. Identifica-se que "a sujeita nos sistemas de placas solares devido à poeira e à neve, e a subsequente diminuição na produção de energia, é o fator de maior impacto sobre a produção do sistema depois da irradiação solar. Especialmente em regiões mais áridas, a sujeira pode afetar significativamente plantas de produção, sendo necessária a mitigação destes efeitos efetuando a limpeza de todo o sistema." (SCHILL, *et al.*, 2022). Os autores indicam também que as regiões com maior irradiação solar, tendo assim maior potencial de produção, são geralmente regiões mais áridas, o que as torna mais suscetíveis a sujeira, causando diminuição da produção. Estas perdas devido à sujeira em painéis solares representaram em 2018 uma diminuição de 4%  da produção anual, correspondendo a uma perda de 3 a 5 bilhões de euros, estimando que este valor poderia chegar a 7 bilhões de euros em 2023.
</div>
<br>
<div style="text-align: justify">
"Interessante saber que a eletricidade produzida pela fonte solar [...] inclui a GD (geração distribuída), ou seja, a geração das placas solares de telhados de casas, shoppings e estacionamentos. A GD vem crescendo bastante no nosso país!" (EPE, 2023). Assim, a produção de um limpador de placas solares de baixo custo, de fácil instalação e operação, visando não somente atender as necessidades instalações de grande porte, mas também as necessidades de um cliente proprietário de uma instalação residencial de pequeno porte, servirá de medida ativa para a mitigação do acúmulo de sujeira em placas solares, em um mercado crescente e ainda carente de soluções automatizadas para realização de limpeza em placas. O investimento neste produto seria rapidamente compensado pelo aumento na produção, e consequentemente na receita, que seria perdida em caso de não realização da limpeza das placas solares.
</div>

# **Levantamento de normas técnicas relacionadas ao problema**

<div style="text-align: justify">
Algumas normas técnicas relacionadas ao problema em questão foram levantadas. Estas tangem principalmente aos temas de instalações elétricas e equipamentos elétricos, segurança ao se trabalhar em instalações elétricas e com equipamentos elétricos, segurança para trabalho em alturas, uma vez que parte das instalações fotovoltaicas são localizadas sobre telhados requerendo boas práticas para garantir a segurança do operador, e normas relativas às boas práticas de software.
</div>

## _NBR 5410 - Instalações elétricas de baixa tensão_

<div style="text-align: justify">
Os padrões e diretrizes para o projeto, execução e manutenção de instalações elétricas de baixa 
tensão, aplicáveis a instalações residenciais, comerciais, industriais e públicas, são definidos pela 
NBR 5410. Inclui instalações de baixa tensão, que podem chegar a 1000 V de tensão alternada e 
1500 V de tensão contínua.
</div>


## _NBR 6148 - Condutores isolados com isolação extrudada em cloreto de polivinila (PVC) para tensões até 750 V_

<div style="text-align: justify">
Estabelece os requisitos de qualificação e aceitação de condutores isolados com cloreto de polivinila 
(PVC) sem cobertura do tipo BWF para tensões de isolamento de até 750 V. Além disso, estabelece a 
capacidade de condução de corrente para diferentes seções nominais de condutores e fornece 
orientação sobre o dimensionamento adequado de instalações elétricas.
</div>

## _NBR NM 280 - Condutores de cabos isolados_

<div style="text-align: justify">
Padroniza seções nominais de 0,5 mm² a 2.000 mm², especificando o número e diâmetros dos fios, 
além de valores de resistência elétrica para condutores de cabos elétricos. 
</div>

## _NR 10 - Segurança em instalações e serviços em eletricidade_

<div style="text-align: justify">
Estabelece requisitos de segurança e saúde para trabalhadores que atuam em instalações e serviços 
que envolvem eletricidade, visando proteger a integridade física dos trabalhadores e reduzir os 
riscos de acidentes elétricos. A norma cobre todas as fases relacionadas à eletricidade, incluindo 
geração, transmissão, distribuição e consumo, bem como as etapas de projeto, construção, 
montagem, operação e manutenção de instalações elétricas. Os principais tópicos abordados pela
mesma são o prontuário das instalações elétricas, medidas de proteção, desenergização, 
reenergização e treinamentos.
</div>

## _NR 12 - Segurança no trabalho em máquinas e equipmentos_

<div style="text-align: justify">
Estabelece referências técnicas, princípios fundamentais e medidas de proteção para garantir a 
saúde e a integridade física dos trabalhadores que lidam com máquinas e equipamentos. Definindo 
requisitos mínimos para a prevenção de acidentes e doenças ocupacionais nas fases de projeto, uso, 
operação e manutenção de máquinas, abrangendo tanto equipamentos novos quanto usados, 
exceto quando há menção específica sobre sua aplicabilidade.
</div>

## _NR 35 - Trabalho em alturas_

<div style="text-align: justify">
Estabelece os requisitos mínimos e as medidas de proteção para o trabalho em altura, visando 
garantir a saúde e a integridade física dos trabalhadores. É aplicável a qualquer atividade que esteja 
acima de dois metros da altura do piso.
</div>

## _NBR 16489 -Sistemas e equipamentos de proteção individual para trabalho em alturas_

<div style="text-align: justify">
Estabelece orientações e recomendações sobre a seleção, uso e manutenção de sistemas de 
proteção individual contra quedas (SPIQ) para uso no local de trabalho, com o objetivo de prevenir 
e/ou reter quedas. A norma ainda explica e define detalhadamente cada equipamento de proteção individual (EPI) indicado para trabalhos 
em altura.
</div>

## _NBR NM 60335-1 - Segurança de aparelhos eletrodomésticos e similares_

<div style="text-align: justify">
A NBR NM 60335-1 é uma norma técnica que trata da segurança de aparelhos eletrodomésticos e 
similares. Ela se aplica à aparelhos cuja tensão nominal não seja superior a 250 V para aparelhos 
monofásicos, e 480 V para outros aparelhos. A norma estabelece os requisitos gerais para garantir a 
segurança desses aparelhos, reduzindo os riscos de acidentes e aumentando a segurança.
</div>

## _IEEE 829 - Padrões para documentação de testes de software e sistemas_
<div style="text-align: justify">
Os processos de teste determinam se os produtos de desenvolvimento de uma atividade específica estão de acordo com os requisitos dessa atividade e se o sistema e/ou software atende ao seu uso pretendido e às necessidades do usuário.</div>
<div style="text-align: justify">
As tarefas do processo de teste são especificadas para diferentes níveis de integridade. Essas tarefas do processo determinam a amplitude e profundidade apropriadas da documentação de teste. Os elementos de documentação para cada tipo de documentação de teste podem então ser selecionados.</div>
<div style="text-align: justify">
O escopo dos testes abrange sistemas baseados em software, software de computador, hardware e suas interfaces. Este padrão se aplica a sistemas baseados em software em desenvolvimento, manutenção ou reutilização (legado, COTS, itens não desenvolvidos). O termo software também inclui firmware, microcódigo e documentação.</div>
<div style="text-align: justify">
Os processos de teste podem incluir inspeção, análise, demonstração, verificação e validação de produtos de software e sistemas baseados em software (IEEE 829, 2008).
</div>

## _IEEE 1012 - Padrões de sistemas e software para validação e verificação_

<div style="text-align: justify">
IEEE 1012 - Standard for System and Software Verification and Validation: Os processos de verificação e validação (V&V) são usados para determinar se os produtos de desenvolvimento de uma determinada atividade estão em conformidade com os requisitos dessa atividade e se o produto satisfaz o uso pretendido e as necessidades do usuário (IEEE 1012, 2017). A norma IEEE 1012 abrange atividades como revisão, auditoria, teste e análise estática, que são fundamentais para garantir a qualidade do software e do sistema. Ela define processos e atividades que podem ser aplicados durante todo o ciclo de vida do desenvolvimento de software e sistemas, desde a concepção até a entrega e manutenção.

</div>

## _ISO 25010 - Requisitos de qualidade e avaliação_
<div style="text-align: justify">
A norma ISO 25010, parte da série SQuaRE (Software product Quality Requirements and Evaluation), estabelece um modelo abrangente para avaliar a qualidade de software. Este modelo é composto por nove características principais, cada uma delas com seus próprios subcaracterísticas.</div>
<div style="text-align: justify">
Essas características incluem funcionalidade, que avalia se o software atende às necessidades declaradas e implícitas dos usuários; desempenho, que considera a eficiência do sistema em termos de tempo de resposta e utilização de recursos; compatibilidade, que verifica a capacidade do software de interoperar com outros sistemas e ambientes; e capacidade de interação, que avalia a facilidade de uso e acessibilidade do sistema para os usuários(ISO 25010, 2020).
</div>


# **Identificação de solução comerciais**

<div style="text-align: justify">
Conforme estabelecido, tem-se que a limpeza de painéis solares é essencial para seu funcionamento eficiente, maximizando a produção de cada instalação. Por se tratar de um mercado com um numero significativo de clientes em potencial, algumas soluções comerciais existem para oferecer este serviço.
</div>
<br>
<div style="text-align: justify">
Estas soluções são bastante abrangentes. Existem empresas que mandam funcionários às instalações para realização manual da do serviço de limpeza de painéis solares. Outras soluções são semi-automáticas, realizando a limpeza de forma automática, mas necessitando da intervenção ativa de um operador para guiar o equipamento manualmente ou remotamente sobre a placa. Já outras soluções são automáticas, nesta categoria identificam-se dois tipos de solução: limpadores automáticos que são fixados as extremidades superior e inferior da placa, se deslocando sobre estas por meio de rodas tratoras enquanto realizam a limpeza, e limpadores automáticos estilo portal, que são fixados às margens desta ou em trilhos externos, e se deslocam em apenas um eixo para realização da limpeza. Destaca-se que algumas das soluções encontradas realizam apenas limpeza a seco, enquanto outras têm, além desta opção, a de realização de limpeza com água.
</div>
<br>
<div style="text-align: justify">
Nenhuma das soluções encontradas une todos os requisitos do projeto proposto em um único limpador.
</div>
<br>
<div style="text-align: justify">
A seguir apresenta-se algumas das soluções encontradas, com foco na solução mais interessante de cada categoria, excluindo-se a categoria de trabalho manual, com uma breve descrição de seu funcionamento segundo dados publicamente disponíveis, bem como uma tabela que estabelece alguns critérios para realização de um comparativo entre as soluções comerciais encontradas e a proposta do grupo.
</div>

## **Solução 1 - Solar Bot: S-Bot**

<div style="text-align: justify">
O S-Bot é um limpador de placas solares que realiza apenas limpeza a seco, utilizando espanadores rotativos, limpando até 500 móludos por hora. Por não utilizar água pode realizar a limpeza a qualquer momento, porém dependendo do tipo de sujeira presente sobre as placas não será capaz de limpá-las, requerendo a intervenção manual de um operador para retirada desta sujeira.
</div>
<br>
<div style="text-align: justify">
Este limpador é instalado sobre as placas e se desloca em apenas um eixo utilizando rodas que se apoiam sobre elas.
</div>
<br>

**Figura 1: Limpador S-Bot da Solar Bot**

![sbot](assets/images/sbot.jpg)

Fonte: Solar Bot, c2024

## **Solução 2 - CleanX Solar: S4000**

<div style="text-align: justify">
Esta solução é similar à anterior. Um limpador de placas solares que utiliza espanadores rotativos para limpeza das placas exclusivamente a seco. Assim esta solução apresenta as mesmas desvantagens que a anterior.
</div>
<br>
<div style="text-align: justify">
Esta opção tem o diferencial de ter um sistema de monitoramento em tempo real do funcionamento e _status_ sobre a limpeza, porém para que esta funcionalinade seja habilitada o sistema requer a instalação de módulos externos com câmeras e aparato para comunicação.
</div>
<br>

**Figura 2: Limpador S4000 da CleanX Solar**

![s4000](assets/images/S4000.png)

Fonte: CleanX Solar, c2024

## **Solução 3 - Solar CleanX: B1**

<div style="text-align: justify">
O limpador B1 é um lipador do tipo portal, que se desloca em apenas um eixo sem necessidade de fixação sobre as placas. Esta solução aumenta a complexidade do produto, que deve ter um lipador de ângulo adaptável que se desloca no interior do portal para garantir o contato constante durante a limpeza. Em contrapartida, esta solução agiliza o processo de limpeza já que como não é fixo às placas, seu deslocamente entre fileiras será mais rápido.
</div>
<br>
<div style="text-align: justify">
Realiza limpeza a seco e com água utilizando cerdas rotativas, limpando uma área equivalente a 8MW de produção diariamente.
</div>
<br>

**Figura 3: Limpador B1 da SolarCleano**

![B1](assets/images/B1.jpg)

Fonte: SolarCleano, c2024

## **Solução 4 - Solar CleanX: M1**

<div style="text-align: justify">
O limpador M1 é um lipador do tipo carrinho, controlado de forma ativa e remota por um operador. Reasliza limpeza a seco e úmida com cerdas rotativas. Requer conexão a uma mangueira, com adaptador comum de jardim.
</div>
<br>
<div style="text-align: justify">
Para instalações com placas em baixa inclinação o limpador pode ser posicionado diretamente sobre a placa, sem necessidade de fixação, o que faz com que seja ágil e versátil. Há um módulo opcional de transporte, um segundo robô cuja função é recolher o limpador e levá-lo à proxíma sequencia de placas que serão limpas. Porém quando a inclinação das placas é maior este requer uma fixação adicional que adiciona complexidade à sua operação e inviabiliza a utilização do módulo adicional de transporte.
</div>
<br>

**Figura 4: Limpador M1 da SolarCleano**

![M1](assets/images/M1.png)

Fonte: SolarCleano, c2024

## **Tabela comparativa**

**Tabela 1: Comparativo entre soluções comerciais encontradas e a solução proposta pela equipe**

|            **Critérios**            | **Solução 1** <br> Solar Bot - S-Bot |      **Solução 2** <br> CleanX Solar - S4000      | **Solução 3** <br> Solar Cleano - B1 |                                   **Solução 4** <br> Colar Cleano M1                                   |                **Nossa Proposta** <br> Helios CW1               |
|:-----------------------------------:|:-------------------------------:|:--------------------------------------------:|:-------------------------------:|:-------------------------------------------------------------------------------------------------:|:------------------------------------------------:|
| Tipo de limpeza                     | Seco                            | Seco                                         | Úmido e Seco                    | Úmido e Seco                                                                                      | Úmido e Seco                                     |
| Abastecimento com água              | ー                              | ー                                           | Tanque imbutido                 | Adaptador comum de mangueira de jardim                                                            | Adaptador comum de mangueira de jardim           |
| Monitoramento em tempo real         | Não                             | Sim (requer instalação de módulos separados) | Não                             | Sim (requer operador para controle remoto)                                                        | Sim (requer conexão à internet para habilitação) |
| Fixação                             | Sobre a placa                   | Sobre a placa                                | Tipo portal sobre rodas         | Robô carrinho sobre placa (necessita de fixação adicional para instalações com grande inclinação) | Sobre placa                                      |
| Empresa nacional                    | Sim                             | Sim                                          | Não (Luxemburgo)                | Não (Luxemburgo)                                                                                  | Sim                                              |
| Alimentação                         | Não identificado                | Baterias + Solar                             | Baterias                        | Bateria                                                                                           | Bateria                                          |
| Tipo de rolo de limpeza             | Panos rotativos                 | Panos rotativos                              | Escova rotativa                 | Escovas Rotativas                                                                                 | Panos rotativos                                  |
| Número de rolos de limpeza          | 1                               | 1                                            | 1                               | 2                                                                                                 | 2                                                |
| Massa sobre painel                  | Não identificado                | 39 kg                                        | Não se aplica                   | 37 kg                                                                                             | até 25kg                                            |
| Tempo de funcionamento entre cargas | Não identificado                | 2 horas                                      | 5 horas                         | 3 horas                                                                                           | ~2 horas                                         |

# **Objetivo geral do projeto**

<div style="text-align: justify">
O objetivo geral deste projeto é desenvolver e construir um protótipo de um limpador de painéis solares de fácil utilização para limpar as placas sempre que for identificada a necessidade, bem como todo o sistema de controle, software necessário para operar o limpador e a documentação associada.
</div>

# **Objetivo específicos do projeto**

<div style="text-align: justify">
Os objetivos específicos deste projeto são:
</div>
<br>

- Desenvolvimento e construção da estrutura de baixo custo do limpador de painéis solares, que permita sua fixação sobre a placa e translação em uma direção;
- Desenvolvimento e montagem da parte elétrica e eletrônica do limpador que permitirão seu deslocamento com controle de velocidade, bem como o acionamento dos limpadores e controle da vazão de água;
- Desenvolvimento dos programas de controle, bancos de dado e interface de usuário para operação remota do limpador;
- Integração das partes desenvolvidas para construção de um protótipo funcional;
- Criar os manuais de montagem, de usuário e de manutenção do produto.
