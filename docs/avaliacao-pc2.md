# Autoavaliação Ponto de Controle 2

**Tabela 26: Autoavaliação dos integrantes do grupo para o PC2**

| Integrante | Matrícula | Contribuições | Nota |
| :-------: |:---:| :-------------: | :---: |
| Victor Hugo Oliveira Leão | 200028367 | Atuei como diretor de software, criando e administrando todas as issues da equipe de software. Estive presente nas decisões de desenvolvimento, além de apontar para os desenvolvedores pontos de ataque e melhorias. Portanto, avalio que estive presente ponta a ponta no trabalho. | 10 |
| Vinicius Assumpcao de Araújo  | 200028472 | Atuei como parte do time de desenvolvimento, especificamente em relacao ao app do projeto,atuando nas paginas iniciais, especoficas de cada placa e na conexao com o banco de dados e demais conexoes com o backend | 10 |
| João Pedro de Camargo Vaz  | 200020650 | Atuei como parte do time de desenvolvimento, especificamente em relacao ao app do projeto, desenvolvido em flutter, atuando nas paginas iniciais, específicas de cada placa e na conexao com o banco de dados e demais conexoes com o backend | 10 |
|Gabriel Barbosa Pfeislticker de Knegt| 180101056 | -|10|
|Daniel Alberto dos Santos Filho|180030990|Atuei no desenvolvimento do componente estrutural e avaliação de requisitos funcionais.|10|
|Matheus Matos Fernandes|170111156|Atuei na adequação solicitada pelos professores, na composição do dimensionamento dos fios, montagem e diagrama unifilar, além dos testes e validação dos propostos|10|
|Cícero Barrozo Fernandes Filho|190085819|Atuei no desenvolvimento do controle do motor e integração da esp com o app. Também participei da integração da eletrônica com o software, participando de todos os testes relacionados a essa área.|10|
|Christian Fleury Alencar Siqueira|190011602|Atuei no desenvolvimento da tela de limpador, criação do serviço de MQTT e na comunicação com a ESP, implementação das logicas de limpeza e persistência no banco de dados do app.|10|
|Bernardo Chaves Pissutti|190103302|Atuei no desenvolvimento do aplicativo corrigindo bugs e adicionando o código que será usado para a visualização dos status dos mecanismos do sistema.|10|
|Thiago Siqueira Gomes|190055294|-|10|
|Matheus Soares Arruda|190093480|Atuei no desenvolvimento do Software Embarcado, fiz a estrutura inicial do projeto, programei os sensores de fim de curso e construi o código do MQTT|10|
|Guilherme Sanchez Dutra|180113500|Atuei na adequação solicitada pelos professores, na composição do dimensionamento dos fios e diagrama unifilar, além dos testes e validação dos propostos|10|
| Gabriel Roger Amorim da Cruz  | 200018248 | Atuei como parte do time de desenvolvimento, especificamente em relação a esp, atuando no desenvolvimento do controle da válvula solenoide | 10 |
| João Pedro de Camargo Vaz | 200020650 | - | 10 |
|Patrick Christian de Melo|180036432|-|10
|Lucas Pinheiro de Souza|160156866|-|10
