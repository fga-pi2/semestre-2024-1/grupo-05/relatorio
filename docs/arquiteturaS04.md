# **Arquitetura do Software**

## Introdução

<p style="text-align: justify;">O diagrama apresentado ilustra a arquitetura de software de um sistema automatizado para limpeza de placas solares. O sistema é composto por diversos componentes que trabalham em conjunto para garantir a eficiência e a segurança da operação.</p>

**Figura 10: Arquitetura de Software**

![arquitetura_software](./assets/images/arquitetura-software.jpeg)
<p align="center">Fonte: Autoria própria</p>


## Funcionamento Geral da Arquitetura

1. **Coleta de dados:** Atuadores geram informações sobre o funcionamento.
2. **Processamento dos dados:** Esp32 processa os dados e envia comandos para os atuadores.
3. **Atuação dos atuadores:** Atuadores executam a limpeza das placas solares.
4. **Comunicação com o frontend:** Mosquitto mqtt comunica-se com o App em flutter.
5. **Dados persistidos:** O Sqlite será responsável por persistir dados.
6. **Visualização das informações:** App apresenta as informações para os usuários.

## Tecnologias

### ESP32 (Controlador) - ESP/IDF | ARDUINO IDE 
#### Descrição:

<p style="text-align: justify;">A ESP32 é uma placa microcontroladora Wi-Fi + Bluetooth de alta performance e baixo custo, desenvolvida pela Espressif Systems (ESPRESSIF SYSTEMS, 2016). Possui um processador dual-core Xtensa LX106 de até 300 MHz, Wi-Fi 802.11 b/g/n, Bluetooth 4.2 BLE, Bluetooth Low Energy, GPIOs, ADCs, DACs, interfaces de comunicação serial e diversas outras funcionalidades. Sua arquitetura modular e escalável a torna ideal para diversas aplicações, como controle de dispositivos, automação residencial, IoT e projetos de prototipagem. </p>

#### Objetivo:

<p style="text-align: justify;">No contexto deste projeto, a ESP32 atua como o cérebro do sistema de controle de mecanismos. Suas principais funções incluem:</p>

* Leitura de dados: Receber dados de sensores ou outras fontes de entrada, como botões, interruptores ou interfaces de comunicação serial.
* Processamento de dados: Interpretar e processar os dados recebidos, aplicando lógica de controle e algoritmos específicos para cada aplicação.
* Controle de mecanismos: Enviar comandos para os mecanismos (motores, solenoides, etc.) com base nos dados processados, controlando seu movimento e comportamento.
* Comunicação MQTT: Enviar e receber mensagens MQTT para interagir com o backend do sistema, transmitindo dados de sensores, status dos mecanismos e comandos de controle.

### MQTT (Message Queuing Telemetry Transport)
#### Descrição:

<p style="text-align: justify;">O MQTT (Message Queuing Telemetry Transport) é um protocolo de comunicação leve e eficiente, projetado para conectar dispositivos em redes com recursos limitados, como sensores, atuadores e microcontroladores (ECLIPSE FOUNDATION, 2014). Ele utiliza um modelo de publicação/assinatura, onde os dispositivos publicam mensagens em tópicos específicos e outros dispositivos se inscrevem para receber essas mensagens. O MQTT oferece diversas vantagens que o tornam ideal para aplicações de IoT, incluindo:</p>

* Baixo consumo de energia: O MQTT é otimizado para redes com recursos limitados, utilizando mensagens curtas e eficientes.
* Confiabilidade: O MQTT oferece diversas opções de entrega de mensagens, como QoS (Quality of Service), garantindo que as mensagens sejam entregues de forma confiável.
* Escalabilidade: O MQTT pode suportar um grande número de dispositivos conectados em uma rede, tornando-o ideal para aplicações de IoT em larga escala.
* Segurança: O MQTT oferece opções de segurança, como autenticação e criptografia, para proteger os dados transmitidos na rede.

#### Objetivo:

No contexto deste projeto, o MQTT é utilizado para:

* Comunicação entre a ESP32 e o backend: A ESP32 publica dados de sensores e status dos mecanismos em tópicos MQTT específicos, e o backend se inscreve para receber essas mensagens.
* Comunicação entre o backend e a interface do usuário: O backend pode publicar mensagens em tópicos MQTT para atualizar a interface do usuário com informações em tempo real sobre o estado dos mecanismos.

### Frontend-Backend/App em Flutter
#### Descrição:

<p style="text-align: justify;">O Flutter é um framework de desenvolvimento de interface de usuário móvel multiplataforma criado pelo Google (Flutter, 2017). Ele permite a criação de interfaces de usuário ricas e responsivas para aplicativos móveis iOS e Android usando uma única base de código.</p>

#### Objetivo:
O objetivo principal do frontend em Flutter neste projeto é fornecer uma interface de usuário interativa e intuitiva que permite ao usuário:

* Monitorar o sistema em tempo real: Visualizar dados de atuadores, status dos mecanismos e outras informações relevantes para ter uma visão geral do sistema.
* Controlar os mecanismos: Enviar comandos para ligar, desligar, ajustar parâmetros e controlar o comportamento dos mecanismos de forma remota.
* Analisar dados históricos: Visualizar gráficos e relatórios que representem dados históricos, auxiliando na identificação de tendências e padrões de comportamento do sistema.
* Gerenciar alertas e notificações: Receber alertas sobre eventos importantes, como falhas de dispositivos ou condições anormais, permitindo a tomada de ações corretivas prontamente.
* Usar a memória do nativa do celular para guardar  alguns dados.

### SQLite (Banco de Dados Local)
#### Descrição:

<p style="text-align: justify;">SQLite é uma biblioteca em linguagem C que implementa um banco de dados SQL embutido. É um sistema de gerenciamento de banco de dados relacional completo, contido em uma pequena biblioteca, tornando-o adequado para aplicativos móveis e embarcados (SQLite, 2001). Uma de suas principais vantagens é sua leveza e facilidade de integração em aplicativos, pois não requer um processo de configuração ou administração separado.</p>

#### Objetivo:

<p style="text-align: justify;">No contexto deste projeto, o SQLite é utilizado para:</p>

* Persistência de dados localmente: Armazenar dados de forma local nos dispositivos móveis, garantindo acesso rápido e eficiente às informações mesmo em condições de conectividade limitada ou offline.
* Cache de dados: SQLite pode ser usado como uma camada de cache para dados frequentemente acessados, reduzindo a necessidade de consultas repetitivas ao servidor remoto e melhorando o desempenho geral do aplicativo.
* Suporte a funcionalidades offline: Permitir que o aplicativo continue funcionando normalmente, mesmo quando não há conectividade com a internet, sincronizando posteriormente os dados com o servidor quando a conexão for restabelecida.
* Gerenciamento de dados locais: Oferecer recursos de consulta e manipulação de dados locais, permitindo que o aplicativo realize operações CRUD (Create, Read, Update, Delete) em seu próprio banco de dados interno.
