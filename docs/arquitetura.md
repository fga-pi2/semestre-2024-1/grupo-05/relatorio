# **Arquitetura geral da solução**

O projeto propõe solução para o problema a partir da sinergia de componentes mecânicos, que possibilitam a movimentação adequada da estrutura, com a colaboração de um sistema de injeção de água para facilitar a limpeza. O controle dos componentes mencionados é configurado pelo usuário por meio de aplicação mobile e executado pelo subsistema eletrônico de controle e comunicação, e todo o trabalho realizado só é possível pela presença do subsistema elétrico embarcado na estrutura.

**Figura 5: Arquitetura geral da solução**

![arquitetura_geral](./assets/images/pi2-Geral.png)




