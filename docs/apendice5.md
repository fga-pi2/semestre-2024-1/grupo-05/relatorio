# **Apêndice 05 - Documentação de software**

## Diagrama de Sequência

O diagrama de sequência é uma representação gráfica que descreve a interação entre objetos em um sistema, mostrando a ordem das mensagens trocadas ao longo do tempo. Ele é usado para visualizar o fluxo de controle entre diferentes partes de um sistema.

O diagrama de sequencia a seguir representa o fluxo de controle referente à realização do processo de limpeza da placa.

**Figura 50: Diagrama de sequência**

![Diagrama de Sequência](assets/images/diagrama-sequencia.png)


## Diagrama de Componentes

O diagrama de componentes é uma representação visual da estrutura de componentes de um sistema, mostrando como os componentes interagem entre si e com outros elementos do sistema, como interfaces, bibliotecas e frameworks. Ele ajuda a entender a arquitetura geral do sistema e a visualizar como os componentes estão distribuídos e se comunicam.

Na Figura 51, é possível visualizar o diagrama de componentes mostrando o fluxo de seleção de limpeza, começado no componente da aplicação mobile, que será construída em flutter e iniciará a seleção do tipo de limpeza, para que assim possa ser configurada na placa esp32 que, internamente, selecionará as métricas previamente mapeadas para aquela configuração. A figura 51 ilustra o diagrama de componentes do nosso sistema.

**Figura 51: Diagrama de componentes**

![Diagrama de Componentes](assets/images/diagrama-componentes.png)

## Diagrama de Pacotes

Um diagrama de pacotes é um tipo de diagrama de estrutura estática que mostra como os elementos de um sistema estão organizados em pacotes e como esses pacotes estão interconectados. Ele ajuda a visualizar a arquitetura de um sistema, mostrando a estrutura de pacotes e as dependências entre eles. Os pacotes representam grupos lógicos de elementos, como classes, interfaces e outros pacotes. As setas entre os pacotes indicam dependências, mostrando a direção do fluxo de dependência entre os pacotes. O diagrama de pacotes é útil para entender a organização geral de um sistema e suas dependências principais. A figura 52 ilustra o diagrama de pacotes do nosso sistema.

**Figura 52: Diagrama de pacotes**

![Diagrama de Pacotes](assets/images/diagrama-pacotes.png)


### Pacote Flutter

O pacote flutter está representando o nosso aplicativo mobile que será construído para gerenciar as placas que o usuário tem conexão. Logo acima, os primeiros pacotes representam as pastas que serão implementadas fora do pacote lib:

- IOS e ANDROID: São pacotes necessários para configuração de algumas bibliotecas,
  no caso da biblioteca mqtt_client é necessários ajustar algumas configurações nessas pastas. Esses pacotes também são elementos padrões usados para geração dos códigos de cada plataforma.
- mqtt_client e sqflite: São pacotes externos que vamos utilizar para a implementação do sistema, o mqtt_client será usado para acessar o protocolo mqtt com a esp32, já o sqflite será usado para gerir a base de dados configurada em Sqlite.

### Pacote Lib

O pacote lib é a pasta onde estará o código fonte construído pelos desenvolvedores, como páginas, controle de estados e importação de bibliotecas.

- Controllers: É onde os estados da aplicação serão gerenciados.
- Pages: Onde a página será construída.
- Mqtt_service: Onde os métodos de comunicação mqtt serão construídos, como subscribe em canais, envio e recebimento de mensagens.
- Repository: Pacote que fará contato com a camada de persistência, nele serão construídos métodos que, utilizando a biblioteca sqflite, vão se comunicar com o banco.

### Pacote EPS32

Representa nosso microcontrolador que, além de fazer o controle de válvulas e de motores, também implementará a funcionalidade de mqtt.

- ArduinoIDE: É um ambiente de desenvolvimento que será usado para implementar as funcionalidades da EPS32.

### SQLITE

É o pacote que representa nossa base de dados.

- devices: Tabela que será populada com os dispositivos que o usuário salva pelo aplicativo.

## Diagrama de Casos de Uso

Um diagrama de caso de uso descreve as interações entre os usuários (ou atores) e um sistema de software. Ele é frequentemente usado na fase de design de sistemas para capturar os requisitos do sistema de forma visual.

**Figura 53: Diagrama de Casos de Uso**

![Diagrama de caso de uso](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-05/relatorio/-/raw/main/docs/assets/images/diagrama-caso-de-uso.jpg)


### Descrição dos Casos de Uso

**Tabela 18: Descrição dos casos de uso**

| UC01 | Informações |
| :--: | :---------: |
| Descrição | O usuário cadastra um limpador |
| Ator | Usuário |
| Pré-condições | Acesso ao app |
| Ação | O usuário insere o nome e o id de um novo limpador |
| Fluxo Principal | O usuário abre o app<br/>O usuário clica no botão de cadastrar novo limpador<br/>O usuário escreve o nome e o id do novo limpador<br/>O usuário conclui o cadastro |
| Exceções | O usuário cadastrar um nome ou id já existentes: deve-se inserir valores válidos |
| Pós-condições | O usuário tem acesso ao novo limpador para manipulá-lo |

| UC02 | Informações |
| :--: | :---------: |
| Descrição | O usuário seleciona a limpeza de cada limpador |
| Ator | Usuário |
| Pré-condições | Acesso ao app<br/>Limpador já cadastrado |
| Ação | O usuário seleciona entre opções já possíveis, qual tipo de limpeza quer para uma placa específica |
| Fluxo Principal | O usuário abre o app<br/>O usuário clica no limpador que desejar<br/>O usuário clica na limpeza desejada para o limpador |
| Exceções | O usuário tentar selecionar o tipo de limpeza já selecionado: não se faz diferença para o app |
| Pós-condições | O usuário escolhe o novo tipo de limpeza para o limpador selecionado |

| UC03 | Informações |
| :--: | :---------: |
| Descrição | Aciona limpeza apenas para um limpador |
| Ator | Usuário |
| Pré-condições | Acesso ao app<br/>Limpador já cadastrado<br/>Tipo de limpeza escolhida |
| Ação | O usuário aciona a limpeza apenas para o limpador escolhida |
| Fluxo Principal | O usuário abre o app<br/>O usuário clica no limpador que desejar<br/>O usuário clica em acioanr limpeza |
| Exceções | - |
| Pós-condições | O limpador inicia a limpeza e o temporizador inicia na tela do app |

| UC04 | Informações |
| :--: | :---------: |
| Descrição | O usuário aciona a limpeza para todos os limpadores |
| Ator | Usuário |
| Pré-condições | Acesso ao app<br/>Limpador(es) já cadastrado(s) |
| Ação | O usuário aciona a limpeza para todos os limpadores |
| Fluxo Principal | O usuário abre o app<br/>O usuário clica na opção de iniciar limpeza para todos os limpadores |
| Exceções | O usuário não ter nenhum limpador cadastrado: é necessário cadastrar um limpador |
| Pós-condições | Os limpadores iniciam a limpeza e o temporizador inicia na tela do app |

## Backlog

Nesta subseção é apresentado o Backlog criado para alinhamento das necessidades dos usuários definidos como público alvo, apresentado na Tabela 10. Além disso, cada História de Usuário foi priorizada utilizando a técnica MoSCoW, que é usada tanto para gestão, análise de negócios, quanto para o desenvolvimento de software e gerenciamento de projetos. Ele faz a categorização das histórias de usuários com os seguintes parâmetros:

 1. Must have: compreende os requisitos que são indispensáveis, e possui prioridade máxima no desenvolvimento.
 2. Shoul have: compreende os requisitos que são importantes, porém não são vitais do ponto de vista estratégico para o produto final.
 3. Could have: compreende os requisitos desejáveis, mas não essenciais do ponto de vista da entrega e satisfação do cliente.
 4. Wouldn’t have: compreende os requisitos menos críticos, com menor retorno do in- vestimento do produto final, podendo ser realizados posteriormente.

 **ES0 - Épico de software**<br>
 **FS0 - Feature de software**<br>
 **US0 - História de usuário de software**


### Épico: E01 - Controle de Motores

**Tabela 19: E01**

| Épico | Feature | História de usuário | Priorização | Descrição |
|-------|---------|---------------------|-------------|-----------|
| E01   | F01 - Motores | US01 | Must | Eu, como operador, desejo que o sistema controle o motor de passo que desloca o limpador para realizar a limpeza de forma uniforme. |
| E01   | F01 - Motores | US02 | Must | Eu, como operador, desejo que o sistema ajuste a velocidade do motor de passo que gira os tubos limpadores conforme a necessidade de limpeza. |
| E01   | F01 - Motores | US03 | Must | Eu, como técnico, desejo que o sistema identifique e informe falhas nos motores para facilitar a manutenção e evitar danos. |
| E01   | F01 - Motores | US04 | Should | Eu, como operador, desejo que o sistema responda dinamicamente a condições variáveis, ajustando os motores para garantir a eficácia da limpeza em tempo ventoso. |
| E01   | F01 - Motores | US05 | Could | Eu, como gestor, desejo que o sistema colete dados sobre o desempenho dos motores para otimizar o consumo de energia e a eficiência da limpeza. |

### Épico: E02 - Controle da Válvula Solenoide

**Tabela 20: E02**

| Épico | Feature | História de usuário | Priorização | Descrição |
|-------|---------|---------------------|-------------|-----------|
| E02   | F02 - Válvula Água | US06 | Must | Eu, como operador, desejo que o sistema controle a válvula solenoide para otimizar o uso de água durante a limpeza. |
| E02   | F02 - Válvula Água | US07 | Should | Eu, como operador, desejo que o sistema ajuste a vazão de água com base no nível de sujeira detectado pelos sensores. |
| E02   | F02 - Válvula Água | US08 | Could | Eu, como técnico, desejo que o sistema realize manutenção preventiva na válvula solenoide, utilizando os dados de operação para prever e prevenir falhas. |

### Épico: E03 - Integração de Sensores

**Tabela 21: E03**

| Épico | Feature | História de usuário | Priorização | Descrição |
|-------|---------|---------------------|-------------|-----------|
| E03   | F03 - Sensores | US09 | Must | Eu, como operador, desejo que o sistema utilize um sensor de fim de curso para indicar o término da limpeza. |
| E03   | F03 - Sensores | US10 | Must | Eu, como operador, desejo que o sistema interrompa a operação automaticamente em caso de detecção de obstáculos. |
| E03   | F03 - Sensores | US11 | Must | Eu, como gestor, desejo que o sistema utilize sensores para coletar dados climáticos e ajustar o ciclo de limpeza de acordo com a previsão do tempo. |
| E03   | F03 - Sensores | US12 | Should | Eu, como técnico, desejo que o sistema alerte para a necessidade de manutenção dos sensores para garantir a precisão e o desempenho. |

### Épico: E04 - Operação e Segurança

**Tabela 22: E04**

| Épico | Feature | História de usuário | Priorização | Descrição |
|-------|---------|---------------------|-------------|-----------|
| E04   | F04 - Segurança | US13 | Must | Eu, como operador, desejo que o sistema tenha funcionalidades de segurança para proteger contra danos causados por mal funcionamento ou condições adversas. |
| E04   | F04 - Segurança | US14 | Must | Eu, como gestor, desejo que o sistema registre os eventos críticos e erros para auditoria e melhoria contínua. |
| E04   | F04 - Segurança | US15 | Should | Eu, como operador, desejo que o sistema possua uma parada de emergência acessível para interromper imediatamente todas as operações em caso de risco iminente. |
| E04   | F04 - Segurança | US16 | Should | Eu, como técnico, desejo que o sistema execute verificações de segurança antes de cada ciclo de limpeza para assegurar a integridade operacional. |


### Épico: E05 - Manutenção e Diagnóstico

**Tabela 23: E05**

| Épico | Feature | História de usuário | Priorização | Descrição |
|-------|---------|---------------------|-------------|-----------|
| E05   | F05 - Manutenção | US17 | Must | Eu, como técnico, desejo que o sistema realize autodiagnósticos regulares para antecipar a necessidade de manutenção. |
| E05   | F05 - Manutenção | US18 | Should | Eu, como operador, desejo que o sistema forneça instruções de manutenção passo a passo via interface para facilitar pequenos reparos e procedimentos regulares. |
| E05   | F05 - Manutenção | US19 | Could | Eu, como técnico, desejo que o sistema mantenha um histórico detalhado de manutenção para acompanhar o desgaste dos componentes ao longo do tempo. |
| E05   | F05 - Manutenção | US20 | Could | Eu, como gestor de manutenção, desejo que o sistema sugira agendamentos de manutenção com base no uso e desempenho dos componentes. |
| E05   | F05 - Manutenção | US21 | Could | Eu, como desenvolvedor, desejo que o sistema permita atualizações de firmware remotamente para garantir que esteja sempre operando com as últimas melhorias e correções. |

### Épico: E06 - Aplicativo

**Tabela 24: E06**

| Épico | Feature | História de usuário | Priorização | Descrição |
|-------|---------|---------------------|-------------|-----------|
| E06   | F06 - Funcionamento do Aplicativo | US22 | Must | Eu, como usuário, desejo cadastrar meus limpadores para que eu possa controlá-los. |
| E06   | F06 - Funcionamento do Aplicativo | US23 | Must | Eu, como usuário, desejo que iniciar a limpeza específica para um limpador ou para todos para que eu possa ter autonomia sobre os limpadores. |
| E06   | F06 - Funcionamento do Aplicativo | US24 | Should | Eu, como usuário, desejo que o aplicativo me mostre um contador de quanto tempo falta para terminar a limpeza para que eu possa me programar. |
| E06   | F06 - Funcionamento do Aplicativo | US25 | Should | Eu, como usuário, desejo que eu possa escolher a limpeza de cada placa específica, para que eu possa personalizar a limpeza. |

### Lista de Exceções

Abaixo pode ser encontrada uma lista com as exceções que devem ser tratadas pela parte de Software:

1. Aviso da ESP para o Aplicativo do fim de uma limpeza:

    - A ESP deve avisar ao app quando uma limpeza acabar, pela mensageria MQTT. Assim, o app irá atualizar o status da limpeza do limpador em questão.

2. Parada da limpeza + voltar ao início:

    - Além da parada da limpeza no meio pelo aplicativo, deve-se também criar a funcionalidade de parar a limpeza e voltar o aparelho ao início;
    - Assim, a ESP deve parar a limpeza e voltar o que andou (usar sensores ou armazenar a quantidade de passos dados até o momento).

3. Parada do sistema pelo sensor de fim de curso no meio de uma limpeza (ou no final, por não parar):

    - Avisar ao app a parada repentina (mensagem MQTT);
    - A PRINCÍPIO, apenas parar o limpador, mas EM EVOLUÇÃO, poderia adicionar a sua volta até a posição inicial (onde o sensor de fim de curso inicial acionar).