## **Projeto do sistema mecânico**

O limpador de placas solares Helios CW1 necessita operar dois sistemas mecânicos independentes e de forma simultânea. São eles:

1. Movimentar o limpador sobre a placa solar;
2. Girar os limpadores com RPM suficiente;

Para realizar o movimento descrito nos dois itens acima, foi decidida a implementação de dois motores de passo NEMA 23, os quais serão fixados na chapa de aço superior. O primeiro motor de passo (Fig. 24) será conectado a um polia GT2 de 60 dentes, o qual irá transmitir o torque gerado, a partir de uma correia GT2 de 400mm de comprimento, para as duas rodas fixadas na chapa de aço superior, a quais estarão conectadas a polias GT2 de 20 dentes. Por fim, um tensionador de correias GT2 será utilizado para garantir a tensão correta na correie e evitar que a mesma deslize.

**Figura 24: Vista renderizada do sistema de movimento**

![mecanismo_rodas](assets/images/mecanismo_rodas.PNG)

Uma configuração similar será utilizada para tranferir o torque do segundo motor (Fig. 25) para os limpadores. Isto é, serão utilizados uma polia GT2 de 60 dentes no eixo do motor, uma correia GT2 para tranferir o torque e 2 polias GT2 de 20 dentes fixadas nos limpadores para receber o torque.

**Figura 25: Vista renderizada da transmissão do sistema de limpeza**

![mecanismo_limpador](assets/images/mecanismo_limpador.PNG)

A escolha de utilizar polias e correias GT2 surge do fato de que tais componentes são amplamente utilizados em soluções para mecanismos de impressoras 3D. Como o mecanismo do Helios CW1 se assemelha à aquele de uma impressora 3D (motores de passo, transferência de torque a partir de correias), decidiu-se utilizar a solução pronta do mercado. As dimensões das polias e correias GT2 também atendem às necessidades do projeto, isto é, um mecanismo compacto que possa operar com precisão em baixas velocidades. Uma alternativa para a solução atual seria o de se utilizar motores de corrente contínua e correias automobilisticas, que aumentariam a capacidade de torque e transmissão, mas pecariam no quesito de dimensão.

Após a realização de alguns testes de movimentação da máquina sobre uma placa solar, foi observado que o atrito gerado entre as rodas e os eixos é mínimo e, dessa forma, os 12kgf*cm gerados por cada um dos motores de passo NEMA 23 será mais do que o suficiente para realizar toda a movimentação requerida pela máquina.

<!---

Para o projeto de sistema mecânico é necessário garantir que cada um dos dois motores de passo tenham torque o suficiente para:

1. Movimentar o limpador sobre a placa em tempo hábil;
2. Girar os limpadores com rpm suficiente;

Tendo em vista tais requisitos, foram escolhidos dois motores de passo NEMA 23 com 12,6kgf*cm, devido ao valor do torque para baixos valores de rpm (1 a 60 rpm), relação entre preço e torque favorável comparado à outras opções no mercado, disponibilidade de motores similares por parte da própria UnB-FGA (caso seja possível sua adaptação para o protótipo) e dimensões que atendem ao espaço disponível para sua fixação no aparelho.

O valor exato (ou sequer próximo) do torque necessário para a operação do lavador ainda é desconhecido, devido principalmente à complexa iteração de atrito entre a estrutura não-rígida do pano de microfibra molhado com o vidro sólido da placa solar. Foi decidido, portanto, utilizar os motores disponibilizados pela FGA para testar o movimento da estrutura, uma vez que a mesma estiver construída e, caso se mostre necessário redimensionar o motor, outro NEMA 23 mais ou menos potente pode ser comprado em tempo hábil e substituído no aparato mecânico utilizando os dados obtidos com precisão.

Já para transferir a potência dos motores para as rodas e para os limpadores, foram implementadas polias e correias dentadas GT2, que são peças utilizadas principalmente no mercado de impressoras 3D, as quais garantem uma excelente compatibilidade entre os motores e as diversas partes móveis do protótipo. Para garantir que, durante a montagem, as correias suficientemente tensionadas a fim de evitar possíveis deslizamentos, os furos que conectam os motores à placa metálica serão feitos de maneira que permitam que os parafusos (e consequentemente o motor) sejam ajustados para posição vertical ideal que forneça a tensão necessária às correias.

-->