# **Referências Bibliográficas**

ABNT. **ABNT NBR 5410**: Instalações Elétricas de Baixa Tensão, 2008.

ABNT. **ABNT NBR 6148**: Condutores isolados com isolação extrudada em cloreto de polivinila (PVC) para tensões até 750 V, 2003.

ABNT. **ABNT NBR 16489**: Sistemas e equipamentos de proteção individual para trabalho em alturas, 2017.

ABNT. **ABNT NBR NM 280**: Condutores de cabos isolados, 2011.

ABNT. **ABNT NBR NM 60335-1**: Segurança de aparelhos eletrodomésticos e similares, 2006.

BRASIL. Ministério do Trabalho e Emprego. **NR 10**: Segurança em instalações e serviços em eletricidade, 2019.

BRASIL. Ministério do Trabalho e Emprego. **NR 12**: Segurança no trabalho em máquinas e equipmentos, 2022.

BRASIL. Ministério do Trabalho e Emprego. **NR 35**: Trabalho em alturas, 2022.

CLEANX SOLAR. **Energia limpa de verdade**. c2024. Disponível em <https://www.cleanxsolar.com.br/>. Acesso em: 15 abr. 2024.

ECLIPSE FOUNDATION. **Mosquitto**. Disponível em: https://mosquitto.org/. Acesso em: 26/04/2024.

EMPRESA DE PESQUISA ENERGÉTICA. **Matriz Energética e Elétrica**, c2024. Disponível em <https://www.epe.gov.br/pt/abcdenergia/matriz-energetica-e-eletrica#ELETRICA>. Acesso em: 14 abr. 2022

EMPRESA DE PESQUISA ENERGÉTICA. **Relatório síntese do balanço energético nacional**, 2023. 

ESPRESSIF SYSTEMS. **ESP-IDF Documentation**. Versão estável. Disponível em: https://docs.espressif.com/projects/esp-idf/en/stable/esp32/index.html. Acesso em: 26/04/2024

FLUTTER. **Flutter - Beautiful native apps in record time**. Disponível em: https://flutter.dev/. Acesso em: 26/04/2024.

RELEVANT SOFTWARE. **8 Flutter Advantages for Your Next Project: Why Choose Flutter App Development?**. Disponível em: https://relevant.software/blog/top-8-flutter-advantages-and-why-you-should-try-flutter-on-your-next-project/. Acesso em: 28/05/2024.

SQL EASY. **SQLite Flutter: Unleashing the Power of Databases in Your Apps**. Disponível em: https://www.sql-easy.com/learn/sqlite-flutter/. Acesso em: 28/05/2024.


IEEE Standards Association. **IEEE Standard for Software and System Test Documentation**. Disponível em: <https://standards.ieee.org/ieee/829/3787/>. Acesso em: 13 abr. 2024.

IEEE Standards Association. **IEEE Standard for System, Software, and Hardware Verification and Validation** . Disponível em: <https://standards.ieee.org/ieee/1012/5609/>. Acesso em: 13 abr. 2024.

ISO 25000. **Quality requirements and evaluation (SQuaRE) – Guide to SQuaRE**. Disponível em: <https://iso25000.com/index.php/en/iso-25000-standards/iso-25010\>. Acesso em: 13 abr. 2024.

LEAN INSTITUTE BRASIL. Disponível em: https://www.lean.org.br/. Acesso em: 23 abr. 2024.

MOMBERG,Libni. As vantagens da baterias de lítio: um estudo de caso.TCC (bacharel em engenharia mecânica) - Instituto federal de educação,ciência e tecnologia, universidade de São Paulo. São Paulo, 2022.

NEOENERGIA. **Energia solar cresce mais de 100% em todo o Distrito Federal**, 2022. Disponível em <https://www.neoenergia.com/web/brasilia/w/geracao-energia-solar-dobra-no-distrito-federal>. Acesso em: 14 abr. 2022

Serafim, Luis Gustavo Souza. Desenvolvimento do sistema de gerenciamento de bateria inteligente integrado com ROS. Tese (Mestrado de dupla diplomação) - Universidade Tecnológica Federal do Paraná. Paraná, 2023.

SCHILL, Christian, _et al_. **Soiling Losses** – Impact on the Performance of
Photovoltaic Power Plants. 2022

SOLAR BOT. **Pioneiros no serviço de limpeza de painéis solaes utilizando o 1° rob6e brasileiro, autônomo e sem água.**. c2024. Disponível em <https://www.solarbot.com.br/sobre.html>. Acesso em: 15 abr. 2024.

SOLARCLEANO. **B1: SolarBridge - the future of solar farm cleaning.** c2024. Disponível em <https://solarcleano.com/en/products/solarbridge-solar-panel-cleaning-robot-b1>. Acesso em: 15 abr. 2024.

SOLARCLEANO. **M1: Ultra lightweight and versatile mini solar robot.** c2024. Disponível em <https://solarcleano.com/en/products/mini-solar-panel-cleaning-robot-m1>. Acesso em: 15 abr. 2024.

SOMMERVILLE, Ian - **Engenharia de Software** - 9 ed. - São Paulo: Pearson Prentice Hall, 2011.

SQLite. **SQLite Documentation**. Disponível em: https://www.sqlite.org/. Acesso em: 26/04/2024.

STA - Eletronica: Sistema de gerenciamento de bateria (BMS). Disponível em: https://www.sta-eletronica.com.br/artigos/baterias-em-geral/informacoes-basicas/sistema-de-gerenciamento-de-bateria-bms. Acesso em: 12/05/2024
