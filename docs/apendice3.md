
# **Apêndice 03 - Diagramas elétricos e eletrônicos**

Diagramas elétricos e eletrônicos do sistema: sendo compostos por
diagramas unifilares/trifilares (com os dispositivos de proteção,
seccionamento, seção de fios, etc.) de sistemas de alimentação,
diagramas esquemáticos de circuitos eletrônicos (com identificação dos
componentes eletrônicos que serão utilizados nos circuitos), diagramas
detalhando barramentos de alimentação dos circuitos eletrônicos (ou
seja, trata-se da interface entre sistemas de alimentação e circuitos
eletrônicos), diagramas com detalhes de lógicas e protocolos de
comunicação entre elementos (microcontrolador com microcontrolador,
microcontrolador e sensor, microcontrolador e atuador, microcontrolador
e software, etc);


**Arquitetura de subsistema elétrico**
 

O esquema apresentado abaixo ilustra a arquitetura do subsistema elétrico.

**Figura 48: Arquitetura do subsistema elétrico**

![identificador](assets/images/arq-Energia.png)



O objetivo deste subsistema é o fornecimento de alimentação elétrica para todos os outros subsistemas do limpador. Segundo os requisitos do projeto, o subsistema elétrico deverá garantir esse fornecimento elétrico utilizando uma bateria, cujo dimensionamento mantenha em funcionamento todos os componentes dos outros subsistemas por um período de duas horas.

É imprescindível também garantir o bom isolamento deste subsistema, uma vez que a estrutura do limpador será metálica e terá contato com água, potencializando o risco de choques elétricos. É necessário que haja alguma forma de desenergizar o equipamento para transporte e manutenção deste. Assim um interruptor deve ser capaz de isolar completamente a bateria dos demais componentes elétricos e eletrônicos do limpador.


**Diagrama Unifilar**

**Figura 49: Diagrama unifilar**

![identificador](assets/images/Diagrama_unifilar_v2.jpg)

Foi utilizado um push button de parada emergencial, normalmente fechado, em série com o interruptor para que nos casos críticos seja necessário apenas apertar o botão externo, foi utilizado o ícone da ESP 8266 apenas a fim de representar a ESP 32 por falta do ícone apropriado. O software utilizado para a criação do diagrama foi o QEletroTech.

