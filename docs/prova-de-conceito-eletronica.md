# **Projeto do subsistema de eletrônica**

A seguir apresentam-se os diversos elementos que compõe o projeto do subsistema de eletrônica.

## **Descrição dos componentes eletrônicos**

Motor de Passo (23KM-K711-P2V): O motor 23KM-K711-P2V é um motor de passo que possui a configuração unipolar, ou seja, existem duas bobinas com center tap que quando alimentadas forçam o alinhamento dos conjunto de ímãs acoplados ao eixo do motor gerando o passo. Este modelo por conter o center tap, permite diferentes variações de conexões do motor, de forma a exercer maior ou menor torque a depender da velocidade de operação.


Driver (TB6600): o driver TB6600 é circuito integrado que atua como uma fonte de corrente contínua, este tem como função alimentar os motores de passo garantindo que a corrente correta seja fornecida para cada bobina. Também atua no controle de travamento, inversão de rotação e habilitação, garantindo que os motores sejam completamente desligados caso necessário.


Microcontrolador(ESP32 WROOM): a ESP32 WROOM é uma placa que contém o circuito integrado microcontrolador que será responsável pelo gerenciamento de todas as ações realizadas pelo limpador. A movimentação dos motores, a ativação do sistema hidráulico, o monitoramento dos sensores de fim de curso e conexão para controle remoto, uma vez que possui módulos bluetooth e wi-fi.


Conversor Buck (LM2596): O conversor buck é um regulador de tensão chaveado que permite a redução da tensão de alimentação 12V para a tensão de 3.3V a qual será necessária para o microcontrolador, sensores de fim de curso e o módulo relé.

Válvula Solenoide: a válvula solenoide é um dispositivo eletromecânico que ao ser alimentado, energiza um bobina a qual atua como um eletroímã, retraindo um pino metálico que altera a posição da válvula. Esta será utilizada para permitir ou bloquear a passagem de água no sistema.

Módulo Relé: O módulo relé consiste em outro dispositivo eletromecânico com a adição de um circuito controlado por nível lógico, com isso o sinal enviado pelo microcontrolador liga ou desliga o relé, o qual servirá como controle de ativação da válvula solenoide.

Sensor IR : O sensor de obstáculo infravermelho é um dispositivo que emite um pulso de sinal por meio de um fotodiodo que ao ser refletido retorna a um fototransistor. A depender da tensão gerada pelo fototransistor, um circuito comparador é ativado alterando o nível lógico na saída. Este sinal é enviado ao microcontrolador indicando uma alteração na distância percorrida pelo pulso. Desta forma este dispositivo será utilizado como um sensor de fim de curso, evitando que o limpador solar possa se desprender da placa de forma acidental.

## **Especificações dos componentes eletrônicos**

A Tab. 11 sumariza as especificações de tensão e corrente para cada componente utilizado no projeto.

**Tabela 11: Componentes eletrônicos do projeto**

|              Item              | Quantidade | Tensão [V] | Corrente [A] |
|:------------------------------:|------------|------------|--------------|
| ESP32(WROOM)                   | 1          | 5          | 0.260        |
| Motor de Passo (23KM-K711-P2V) | 2          | 12         | 3 [/fase]    |
| Driver (TB6600)                | 2          | 12         | 4            |
| Conversor Buck (LM2596)        | 1          | 12         | 1            |
| Válvula Solenoide              | 1          | 12         | 1            |
| Módulo Relé                    | 1          | 3.3        | 0.005        |
| Sensor IR                      | 2          | 3.3        | 0.020        |

## **Diagrama esquemático geral**

O diagrama abaixo apresenta as conexões entre cada um dos componentes eletrônicos.

**Figura 26: Diagrama eletrônico**

![esquemático](assets/images/sche_pc1.PNG)

# Funcionamento e Justificativas dos Componentes e Dispositivos Eletrônicos

## Regulador de Tensão Linear e Conversor Buck

O projeto será alimentado por uma bateria de 12V, sabendo que o microcontrolador, o módulo relé e sensor de proximidade operam a 5V, se faz necessário a regulação desta tensão.

### Regulador de Tensão Linear

Reguladores de tensão como o LM7805, são dispositivos que utilizam dissipação térmica para regulação da tensão, e podem ser entendidos de forma simplificada com o circuito abaixo.

Para um circuito alimentando por uma tensão DC igual a 12V, temos que:

O diodo zener presente no circuito é o principal elemento regulador de tensão, tomando como exemplo um diodo zener com tensão de polarização igual a 5,6V e considerando uma queda de tensão base-emissor no transistor de 0,7V. Temos que a tensão de saída do circuito regulador pode ser calculada como [![\\ (V_{out} = V_{Z}-V_{BE})](https://latex.codecogs.com/svg.latex?%5C%5C%20(V_%7Bout%7D%20%3D%20V_%7BZ%7D-V_%7BBE%7D))](#_), aproximadamente 4.9V.

Sendo assim, o restante da tensão de alimentação, recai sobre o transistor, aproximadamente 7,1V. Transistores utilizados nesta aplicação possuem uma constante de ganho de correte [![\\ (\beta=I_C/I_B)](https://latex.codecogs.com/svg.latex?%5C%5C%20(%5Cbeta%3DI_C%2FI_B))](#_) alta, logo sabendo que [![\\ (I_{E}=I_{C}+I_{B})](https://latex.codecogs.com/svg.latex?%5C%5C%20(I_%7BE%7D%3DI_%7BC%7D%2BI_%7BB%7D))](#_) podemos aproximar que [![\\ (I_{E}=I_{C})](https://latex.codecogs.com/svg.latex?%5C%5C%20(I_%7BE%7D%3DI_%7BC%7D))](#_) e por consequência [![\\ (I_{E}=I_{L})](https://latex.codecogs.com/svg.latex?%5C%5C%20(I_%7BE%7D%3DI_%7BL%7D))](#_). Isto é, quanto maior for a tensão de entrada em relação a tensão de regulação ou maior a corrente solicitada pela carga, maior será a dissipação de potência realizada pelo transistor.

Por este motivo, dispositivos de regulação linear possuem baixíssima eficiência. Tomando como exemplo um regulador LM7805, o qual possui tensão de saída igual a 5V, alimentado com uma tensão de 12V e uma carga que consome 0.260A (corrente consumida por uma ESP32 operando em próximo a capacidade máxima), podemos calcular de forma aproximada a eficiência de conversão como:

[![\\  \\ \eta=\frac{P_{out}}{P_{in}}=\frac{V_{out}\cdot V_{out}}{V_{in}\cdot I_{in}}=\frac{5\cdot 0,260}{12\cdot (0,260)}=\frac{1,3}{3,12}=0,42.](https://latex.codecogs.com/svg.latex?%5C%5C%20%20%5C%5C%20%5Ceta%3D%5Cfrac%7BP_%7Bout%7D%7D%7BP_%7Bin%7D%7D%3D%5Cfrac%7BV_%7Bout%7D%5Ccdot%20V_%7Bout%7D%7D%7BV_%7Bin%7D%5Ccdot%20I_%7Bin%7D%7D%3D%5Cfrac%7B5%5Ccdot%200%2C260%7D%7B12%5Ccdot%20(0%2C260)%7D%3D%5Cfrac%7B1%2C3%7D%7B3%2C12%7D%3D0%2C42.)](#_)

Isso significa que apenas 42% da potência fornecida pela fonte é entregue a carga, os 58% restantes são dissipados em forma de calor por meio do transistor.

A Fig. 27 abaixo apresenta uma simulação realizada com componentes reais em condições semelhantes as calculadas anteriormente.

**Figura 27: Simulação de regulador de tensão linear**

![Simulação de regulador de tensão linear.](assets/images/LVR.png)

Por conta disso, uma forma alternativa de regulação de tensão foi buscada.

### Conversor Buck

Conversores buck, diferentemente de conversores lineares, utilizam de uma topologia muito similar a uma fonte chaveada. Ou seja, de forma simplificada, um sinal DC é aplicado na entrada do circuito, o qual é filtrado por um capacitor de bypass , reduzindo os transientes do sinal. Em seguida este é convertido em um sinal AC conhecido como PWM, a depender do ciclo de trabalho deste sinal a tensão média na saída será alterada.

Porém esta tensão se mantém alternada e com altos picos. De modo a reduzir os picos de tensão, um indutor é adicionado, uma vez que este componente resiste a variações rápidas de corrente, se a frequência do sinal PWM for alta o suficiente a tensão entre os terminais do indutor pode ser, de certa forma, estabilizada, evitando os picos no sinal. Resolvido os picos, o sinal ainda possui uma componente em alta frequência, logo é adicionado um capacitor o qual atenua as altas frequências retificando ao máximo o sinal de saída que agora se comporta como um sinal DC.

Pelo seu funcionamento, conversores buck se apresentam extremamente eficientes, a depender do CI utilizado a eficiência de conversão pode chegar a 95%, logo esta topologia se apresenta como uma melhor escolha ao projeto.

Neste projeto o conversor buck escolhido se baseia no CI (LM2596s) Segundo o datasheet a eficiência de conversão de 12V para 5V é de 80%.

A topologia de CI (LM2596s) pode ser observada na imagem abaixo:

**Figura 28: Diagrama de bloco funcional do LM2596s**

![Diagrama de bloco funcional do LM2596s.](assets/images/block_diagram.PNG)

Pinagem (descrição e função de cada pino do CI):

- VIN: este pino deve receber a tensão de entrada do circuito, a qual dever ser filtrada por uma capacitor de bypass evitando os transistes do sinal. Assim como deve receber a corrente necessária para o funcionamento correto do CI.
- OUT: este pino fornece a saída do CI, composta por um sinal PWM modulado a 150 KHz com o ciclo de trabalho definido.
- GND: terminal negativo do CI.
- FB (Feedback): este pino é responsável por medir a tensão de saída informando ao CI se esta se encontra correta, completando o feedback-loop.
- ON/OFF: este pino recebe um sinal lógico permitindo o CI ser ligado ou desligado. Quando não necessária esta implementação o pino dever ser aterrado, mantém o CI sempre na posição (ligado).

O circuito apresentado na imagem abaixo corresponde ao circuito real utilizado no projeto.

**Figura 29: Esquemático do conversor buck baseado no CI LM2596s**

![Esquemático do conversor buck baseado no CI LM2596s.](assets/images/buck.png)

A lista abaixo descreve a função de cada componente no circuito:

- C1/C2: são capacitores de bypass (ou filtro) utilizados para reduzir os transientes do sinal de entrada do CI. Estes precisam fornecer corrente se forma rápida ao CI, logo é importante que possuam um alto \textit{RMS current rating}, este parâmetro pode ser entendido como a máxima potência que o capacitor suporta dissipar antes de atingir uma temperatura crítica que danificara o componente. Essa dissipação de energia ocorre pois capacitores reais possuem uma resistência interna (ESR) a qual esquenta ao ser atravessada por uma corrente.
- TRIMPOT(10K)/R1: estes dois resistores formam juntos um divisor de tensão. A tensão resultante no resistor R1 será utilizada pelo comparador no interior do CI a fim de determinar se a tensão de saída está correta. Esta tensão de saída do conversor pode ser calculada por:

[![\\ V_{out}=V_{REF}\cdot (1+\frac{R_{trimpot}}{R1}) \\ ](https://latex.codecogs.com/svg.latex?%5C%5C%20V_%7Bout%7D%3DV_%7BREF%7D%5Ccdot%20(1%2B%5Cfrac%7BR_%7Btrimpot%7D%7D%7BR1%7D)%20%5C%5C%20)](#_)

O valor de [![\\ (V_{REF}=1,23V) \\ ](https://latex.codecogs.com/svg.latex?%5C%5C%20(V_%7BREF%7D%3D1%2C23V)%20%5C%5C%20)](#_) é o valor da tensão de referência do comparador no interior do CI e é fornecido pelo datasheet. A resistência R1 também é fixa, logo a resistência do trimpot será responsável por regular a tensão de saída. 

Logo para uma tensão de saída igual a 5v para uma alimentação de 12V temos que o valor da resistência do trimpot será:

[![\\ R_{trimpot}= ((\frac{5}{1,23})-1)\cdot 330 \simeq 1 \; [K \Omega] \\ ](https://latex.codecogs.com/svg.latex?%5C%5C%20R_%7Btrimpot%7D%3D%20((%5Cfrac%7B5%7D%7B1%2C23%7D)-1)%5Ccdot%20330%20%5Csimeq%201%20%5C%3B%20%5BK%20%5COmega%5D%20%5C%5C%20)](#_)

- L1: é o indutor responsável por atenuar os picos de tensão gerados pelo sinal PWM. O valor da indutância deve ser selecionado de acordo com a corrente máxima de saída, mas também respeitando a relação entre a frequência de chaveamento do CI. Estão listados no datasheet marcas e modelos recomendados para cada aplicação.

- C3: é o capacitor de bypass responsável por amenizar o ripple (ou ondulações) do sinal de saída deixando-o mais retilíneo possível, este efeito ocorre por meio da filtragem das componentes de alta frequência do sinal. Importante ressaltar que L1 e C3 formam um filtro passa-baixa LC, logo o valor da capacitância de C3 deve ser [![\\ (\leq 820uF)](https://latex.codecogs.com/svg.latex?%5C%5C%20(%5Cleq%20820uF))](#_), de modo a não alterar o comportamento do circuito. Outro fator importante recai sobre o ESR do capacitor, este quanto menor, menor será o ripple de saíra, porém em valores extremamente baixos podem resultar em um feedback loop instável, levando o CI a oscilar, sendo assim sevem ser escolhidos com cautela.

- D1: é um diodo retificador schotky, nesta topologia este diodo recebe o nome especial de diodo catch ou flyback. Este diodo se assemelha muito a um diodo retificador comum, porém por ser da categoria schotky ele opera em altas frequência, o que é extremamente importante uma vez que este CI Opera a 150KHz. Sua função no circuito é oferecer um caminho para a corrente que irá fluir do indutor quando o sinal estiver no zero. Ou seja, em todos os momentos que o sinal PWM percorre o nível zero de tensão, o campo magnético armazenado no indutor gera um pico de tensão em seus terminais com o objetivo de manter a corrente. Essa tensão elevada irá utilizar os componentes do circuito como cargas, principalmente os transistores no interior do CI, se estes não suportarem a tensão gerada pelo indutor o CI será danificado. A presença do diodo, prove um caminho de baixa resistência para o corrente gerada, fazendo que a potência seja dissipada nas próprias espiras do indutor.

## Sensores e Módulos

### Sensor de Proximidade IR

O sensor de proximidade atuará como um sensor de fim de curso para o projeto, outros sensores comuns para esta finalidade como sensores tácteis e de Efeito Hall foram descartados, uma vez seria necessário a instalação ou do próprio sensor ou de ímãs em cada placa solar.

O sensor escolhido se baseia no CI (LM393) o qual se comporta com um circuito comparador. Em seu funcionamento básico, o sensor emite um sinal infravermelho por meio de um fotodiodo o qual reflete em alguma superfície e retorna a um fototransistor. Pelo efeito fotovoltaico, o transistor gera uma tensão entre seus terminais a qual é comparada pelo CI a uma tensão de referencia. A depender do resultado a saída do comparador apresenta um sinal lógico alto ou baixo, este sinal é então enviado ao microcontrolador que irá alvaliar a mudança de estado do sensor.

A imagem abaixo apresenta o circuito utilizado para o acionamento do relé.

**Figura 30: Esquemático do sensor de fim de curso baseado no CI LM393C**

![Esquemático do sensor de fim de curso baseado no CI LM393C.](assets/images/IR_sensor.png)

A lista abaixo descreve a função de cada componente no circuito

- R2,R5,R6: são resistores limitadores de corrente, estes limitam a corrente através dos LEDs evitando sua queima. Diodos idealmente possuem resistência igual a zero, logo sem a presença do resistor a corrente tenderia ao infinito, ou seja, um curto.
- D3,D4: são LED indicadores, D3 indica que o circuito está alimentado e D4 indica o nível lógico na saída do comparador. Quando saída se encontra em nível lógico baixo o LED é ligado, logo para o caso complementar, quando em nível lógico alto o LED é desligado.
- D2: é um LED operando na faixa infravermelha, o qual é responsável por emitir o sinal.
- Q1: é um foto transistor, o coletor deste transistor é conectado a entrada não inversora do comparador e emissor ao terminal negativo. A base do transistor fica exposta no encapsulamento de forma a receber o sinal infravermelho transmitido pelo fotodiodo. Esta excitação na base do diodo induz uma corrente entre a base e emissor polarizando o transistor e consequentemente gerando uma tensão entre os terminais de coletor e emissor.
- R4: é um resistor de polarização utilizado para controlar a corrente e tensão fornecida ao coletor do transistor.
- C5,C4: são capacitores que reduzem os transientes tensão de alimentação e da tensão gerada pela transistor, tornando o circuito mais estável.
- Trimpot (10k): é um resistor variável o qual atua como um divisor de tensão, definindo a tensão de referência fornecida ao comparador. Isto é, a depender desta tensão o comparador irá alterar seu estado em para uma tensão maior ou menor fornecida pela fototransistor.
- R3: é conhecido como resistor de pull-up, este é resposável por elevar a saída do comparador a tensão de alimentação quando em nível lógico alto. Uma atenção deve ser tomana na escolha do valor deste resistor, se este possuir uma resitência muito alta, corrente irá diminuir acarretando um saída que pode flutuar entre o nível alto e baixo. Por outro lado, se a resistência for muito baixa a corrente irá aumentar e por consequência a potência dissipada pela resistor também.

### Módulo Relé

A válvula solenoide a qual controla a parte hidráulica do projeto necessita de uma alimentação de 12V para ser acionada. Está válvula muda de estado quando alimentada, logo se faz necessário um sistema de controle de acionamento, para tal será utilizado um módulo relé. Quando alimentado, o relé, fechara o circuito de alimentação da válvula, fornecendo a tensão de 12V que por consequência irá acionar o solenoide permitindo a passagem de água. De forma complementar, quando cortada a alimentação do relé, a passagem de aguá será bloqueada.

**Figura 31: Esquemático do módulo relé utilizado**

![Esquemático do módulo relé utilizado.](assets/images/relay_module.png)

A lista abaixo descreve a função de cada componente no circuito:

- R8: é o resistor limitador de corrente do LED presente no opto-acoplador, mas também do LED de indicação de nível lógico.
- U1: é o opto-acoplador (PC817C), ao ser alimentado o LED no interior componente, a base do foto-transistor é excitada, ou seja, polarizada, permitindo a que a corrente flua do coletor ao emissor do transistor. Estes circuitos são muito utilizado como isoladores galvânicos, isto é, assim como nos transformadores não existe conexão física entre as duas partes do circuito. Porém neste circuito esse efeito só ocorre quando o jumper JP1 não é utilizado, e para este caso se faz necessário o uso de uma fonte dedicada a porção direita do circuito. Como neste projeto o jumper será utilizado, ou seja, existirá uma conexão física entre os dois lados do opto-acoplador, suas características isolantes e de proteção não serão aproveitadas.
- R7: é o resistor de polarização de base do transistor Q1, limitando a tensão e corrente base-emissor. Está tensão só precisa ser suficiente a polarizar o transistor em região de condução.
- Q1: é o transistor BJT que atua como chave para o acionamento do relé, quando polarizada a base ele conduz corrente entre coletor e emissor permitindo a corrente atravessar os terminais do indutor no interior do relé.
- D6: é um transistor retificador atuando como diodo flyback, eliminando os surtos de tensão que ocorrem no momento em que o indutor é desenergizado.
- RL1: é um relé com 3 terminais, o terminar COM (comum) deve ser conectado ao 12V e o terminal NO (normalmente aberto) ao terminal positivo da válvula solenoide. Desta forma a válvula só será acionada quando o relé for energizado.

## Tentes iniciais

Aqui será apresentada a bancada utilizada e os resultados para os testes inciais referentes ao microcontrolador (ESP32), conversor buck, motor de passo e driver.

Este teste teve como principais objetivos:
- verificar o correto funcionamento do conversor buck como alimentação de 5V para a ESP32.
- verificar se o sinal PWM gerado pelo ESP32 está de acordo como sinal programado.
- Verificar se o motor se comporta de maneira correta, ou seja, o travamento do eixo e revolução completa para ambas as direções.

**Figura 32: Bancada de teste com detalhe do sinal PWM produzido pela ESP32 e enviado ao driver**

![Bancada de teste.](assets/images/test_rig.jpeg)

![Osciloscópio apresentando o sinal PWM produzido pela ESP32 e enviado ao driver.](assets/images/scope.jpeg)

A observação do sinal PWM confirmou a correta programação do microcontrolador, uma vez que o código gera um pulso contendo (800*4=3200)us nível lógico alto e o mesmo tempo em nível lógico baixo, ou seja, 50% de ciclo de trabalho. Desta forma, temos um período de onda igual a 6.4ms, confirmado pela medição do osciloscópio.

![Código utilizado no teste do motor.](assets/images/test_code.png)

Este código permitiu o teste de travamento do motor através da alteração do sinal ENA, a mudança de sentido de rotação através do sinal DIR, mas também da velocidade e tempo de operação. Na imagem só foi apresentado o código para uma situação específica, a fim de simplificar o exemplo, porém durante os teste o código foi modificado a atender cada situação.