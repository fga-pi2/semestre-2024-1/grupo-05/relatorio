# **Projeto do subsistema de energia**

A seguir apresentam-se os diversos elementos que compõe o projeto do subsistema de energia.

## **Projeto de alimentação**

**Dimensionamento Bateria**

O uso de bateria no projeto é crucial por vários motivos. Primeiramente é graças a ela que o projeto consegue ter autonomia, para que não precise ficar conectado continuamente à rede elétrica. Este fato é relevante em locais onde a manutenção de usinas solares não pode ser feita com frequência devido à sua localização remota ou devido a outros fatores importantes. A segunda razão é que a bateria oferece flexibilidade de funcionamento, tendo em vista que os melhores horários para limpeza dos painéis segundo os próprios fabricantes são no período da manhã e da tarde, fora do pico de geração das placas, portanto esta adaptabilidade é fundamental para otimizar a eficiência da limpeza. Em suma, a bateria é essencial no que tange eficiência e autonomia no projeto.
Para o projeto a bateria ideal deve contar com 12V e capacidade de 40Ah para obtermos uma autonomia de 2h, que é o horizonte de funcionamento esperado.
Para o cálculo do dimensionamento da bateria foi utilizado um coeficiente de segurança de 1,5, na parte elétrica do projeto, para que haja segurança na integração entre os sistemas eletrônicos e seus requisitos. Um coeficiente de segurança elevado garante uma margem adicional para evitar falhas, garantindo que o sistema possa suportar cargas inesperadas. 

Potência [W] = Carga [Ah] × Tensão Nominal [V]

Energia [Wh] = Potência [W] × Tempo [h]

Carga [Ah] = Energia [Wh] / Tensão Nominal [V]

**Tabela 12: Lista de componentes e suas especificações**

|                           | Tensão [V] | Corrente [A] | Potência [W] |
|---------------------------|------------|--------------|--------------|
| Bateria                   | 12         |              |              |
| Relé                      | 3,3        | 0,005        | 0,0165       |
| Driver 1                  | 9          | 4            | 36           |
| Driver 2                  | 9          | 4            | 36           |
| Redutor de tensão         | 12         | 1            | 12           |
| Válvula solenoide         | 12         | 0,6          | 7,2          |
| Motor 1                   | 4,4        | 3,4          | 14,96        |
| Motor 2                   | 4,4        | 3,4          | 14,96        |
| Microcontrolador          | 3,3        | 0,26         | 0,858        |
| Sensor 1                  | 3,3        | 1            | 3,3          |
| Sensor 2                  | 3,3        | 1            | 3,3          |
| Total 1h de funcionamento | 12v        | 11,865       | 128,5945     |
| 2h de funcionamento              |           | 23,73        | 21,43241667  |
| com coeficiente de segurança(1,5)      |          | 35,595       | 32,148625    |





**Bateria**

Com relação à escolha do tipo de bateria, foi escolhida uma do tipo íon-lítio, já que as mesmas oferecem uma combinação de alta densidade energética, leveza, ciclo de vida elevado, gerenciamento inteligente e aplicações versáteis. Graças a sua capacidade de armazenar uma grande quantidade de energia em um espaço compacto, se tornam ideais para projetos que exigem alta capacidade em dispositivos pequenos. Além disso, sua leveza as torna fáceis de transportar e instalar em diversos cenários. O gráfico abaixo traz um comparativo entre alguns tipos de bateria e infere-se que a de lítio segue sendo a melhor escolha.

**Figura 33: Comparativo entre tipos de bateria**

![identificador](assets/images/grafico_baterias.png)



**Descrição da bateria**

**Figura 34: Foto da bateria selecionada**

![identificador](assets/images/bateria_litio.png)

- Bateria Lítio 12v 40ah
- Dimensões aproximadas 8cm x 34cm x 7cm
- Peso aproximado 2,7Kg
- Com proteção BMS
- Voltagem nominal: 12v
- Voltagem carga completa: 12,6v
- Voltagem de corte mínima: 9v
- Corrente de descarga: 2C

**BMS - sistema de gerenciamento de bateria**

Do inglês “Battery management system” é um sistema eletrônico que gerencia e monitora toda performance de uma bateria. Além de equilibrar as células, o BMS desempenha um papel fundamental na proteção das baterias contra sobrecarga, sobre descarga, sobrecorrente e variações de temperatura, garantindo um desempenho consistente e seguro. A proteção BMS está inclusa na bateria escolhida.

**Dimensionamento fios de alimentação**

Para a escolha dos cabos de alimentação foi-se levado em conta o AWG (American wire gauge) que é uma escala americana normatizada para determinar o tamanho de fios elétricos. Levando em conta que nos circuitos não teremos corrente que ultrapasse 10 amperes. Conseguimos um número AWG, 16, que usando um conversor chegamos num diâmetro de 1.2908mm e área da seção transversal de 1.3087mm^2. Levando esses fatores em consideração, é pertinente usar cabos com seção de 1,5mm^2, o que dá uma margem de segurança a mais para o projeto.

**Carregador**
Esquemático do carregamento
O processo ilustrado na imagem abaixo é um esquema de conexão de uma fonte de alimentação CA para uma bateria, com um sistema BMS intermediário. Aqui está um resumo dos passos: Fonte CA: A energia começa com uma fonte de Corrente Alternada (CA) de 220V. Conversão DC: A fonte CA é convertida para 12V DC por uma fonte específica. Conector DC: A energia DC é então conectada a um Conector DC Fêmea. Sistema BMS: O sistema BMS (Sistema de Gerenciamento de Bateria) monitora e gerencia o estado da bateria. Bateria: Finalmente, a energia é armazenada na Bateria. Este esquema é comum em sistemas que requerem uma fonte de energia estável e gerenciada para baterias. O BMS é crucial para garantir a segurança e eficiência da bateria.

**Figura 35: Diagrana de blocos do sistema de energia**

![identificador](assets/images/carregador.png)

Tempo de recarga
Utilizando um modelo de carregador genérico encontrado facilmente e por baixo custo, tanto na internet, quanto em lojas especializadas, de 12 Volts e uma capacidade de carga de 10A temos, a partir da fórmula abaixo que:
Tempo de recarga = capacidade da bateria*coeficiente de segurança/corrente de carga do carregador
O tempo de carga de uma bateria de 40 Ah seria de aproximadamente 5 horas, sendo indicado o carregamento entre as sessões de limpeza das placas. A escolha de um carregador de 10A, se dá como sugestão do fabricante.

**Tabela 13: Dados da bateria**

| Capacidade bateria[Ah]                          | 40 |
|-------------------------------------------------|----|
| Corrente de carga[A]                            | 10 |
| Tempo de carregamento[h]                        | 4  |
| Tempo de carregamento com coef. De segurança[h] | 5  |


**Acionamento**

O acionamento para inicio da energização dos circuitos, será por meio de um botão "chave alavanca E-TEN1021on/off". Esta chave foi selecionada já que requer força para sua atuação, evitando assim que o limpador seja acionado acidentalmente.

**Figura 36: Chave liga/desliga**

![identificador](assets/images/botao_onoff.jpg)

Para parada emergencial há um "push button LAY37-11", normalmente fechado, em série com o interruptor para que nos casos críticos seja necessário apenas apertar o botão externo para interromper o fornecimento de energia ao sistema. A inclusão deste componente é essencial para que o limpador seja mais seguro, já que este tipo de botão é de fácil acionamento em caso de emergência.

**Figura 37: Botão de emergência**

![identificador](assets/images/botao_de_emergencia.jpg)

**Cenários**

Para 2h de funcionamento:
Para um cenário, onde o limpador funcionará por 2 horas, levando em consideração todos os pontos acima citados, necessitará de 40Ah para manter o projeto em operação.


Para cerca de 20min demonstrativo/aula
Vale salientar que para uma demonstração/pitch não será necessário que o projeto funcione por 2 horas, levando em consideração que será feita a limpeza de apenas uma placa solar, o que pode levar em torno de 20 minutos, portanto será necessária uma capacidade de cerca de 7Ah. Sendo assim o grupo considerará o requisito de funcionamento de 2 horas para a documentação do projeto, porém opta por construir um protótipo com autonomia reduzida visando a diminuição dos custos à equipe.

Para demonstrativo/pitch fit – Feira de inovação tecnológica da FGA
Tendo em vista que devemos possuir um um bom horizonte de funcionamento para demonstrativos na feira de inovação e levando em conta também os custos do projeto, abre-se a possibilidade de ser usado uma fonte chaveada com alimentação constante, o que gera conforto e segurança para a demonstração do projeto, a fonte em questão AC/DC, 12V e 50A. O que gera possibilidade também de ser usada no demonstrativo/aula pelos mesmos motivos.

**Conclusão**

Em suma, o subsistema elétrico é vital para o funcionamento autônomo e eficiente do projeto. A escolha da bateria, usando um coeficiente alto no dimensionamento, garante segurança e margem de funcionamento para o projeto. A possibilidade do uso de outras baterias/fonte chaveada, demonstra a flexibilidade do subsistema projetado, evidenciando alternativas para imprevistos. Por fim, a seção nominal dos fios escolhidos, botão on/off e botão de emergência, trazem à tona a preocupação com a segurança do projeto e do operador.

