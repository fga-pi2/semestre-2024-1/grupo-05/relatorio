# **Arquitetura do subsistema mecânico**

O subsistema mecânico têm como objetivo possibilitar o deslocamento da estrutura sobre o painel solar e proporcionar limpeza mecânica através da rotação de dois eixos com superfície limpante, posicionados nas extremidades laterais da estrutura.

1. Translação da estrutura\\
    O movimento é realizado por meio de um sistema de correia que liga um motor de passo à duas rodas posicionadas na seção central da estrutura.

2. Rotação dos eixos\\
    Os eixos de superfície limpante são rotacionados a partir de única correia que conecta o motor, posicionada na seção superior, aos eixos das extremidades. 

**Figura 7: Arquitetura do subsistema mecânico**

![arquitetura_mec](./assets/images/arq-Mecanica.png)