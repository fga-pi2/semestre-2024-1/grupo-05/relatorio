

# **Concepção e detalhamento da solução**

## **Requisitos Gerais**

Após definição do escopo geral do projeto, afim de atender os objetivos deste, alguns requisitos gerais foram identificados pela equipe técnica. Estes se dividem em requisitos funcionais e não funcionais.

**RFG - Requisitos Funcionais Gerais** <br>
**RNFG - Requisitos Não Funcionais de Gerais** <br><br>

**Tabela 2: Requisitos funcionais gerais do produto**

| ID  | Descrição                                                                                                                                         |
|-----|---------------------------------------------------------------------------------------------------------------------------------------------------|
| RFG1 |        O produto deve realizar a limpeza dos   painéis solares por meio de rolos rotativos de limpeza e jatos d’água                              |
| RFG2 | O produto deve ser capaz de   controlar o fluxo de água que receba por fornecimento externo.                                                      |
| RFG4 | O   dispositivo deverá ser capaz de trasladar longitudinalmente sobre a placa   para realização da limpeza                                        |
| RFG5 | Durante o traslado o dispositivo   deve respeitar os limites dimensão da placa                                                                    |
| RFG6 | O   controle de velocidade de traslado e rotação dos rolos, bem como a distância   de movimentação do dispositivo deve ser controlado remotamente |
| RFG7 | O   produto deve ser alimentado constante por uma bateira durante o seu   funcionamento                                                      |
| RFG8 | O   produto deve ser capaz de operar em placas de inclinação entre 5 e 20 graus                                                                    |
| RFG9 | O   produto deve ser móvel, podendo ser retirado da placa, e sem a necessidade de   fixações externas a ele                                       |

**Tabela 3: Requisitos gerais não funcionais do produto**

| ID   | Descrição                                                                                                                                                                              |
|------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| RNFG1 |        Receber dados da produtividade das placas   e informar ao usuário quando é necessário realizar a limpeza destas                                                                 |
| RNFG2 | O produto deve poder ser colocado e retirado de placas por no máximo dois operadores sem necessidade de desmontar componentes, podendo ser movido entre sequencias de placas de forma rápida.                       |
| RNFG3 | Conseguir   limpar uma sequencia de placas que estejam alinhadas e adjacentes, contando o   número de placas que já foram limpas de forma a parar automaticamente no fim   da limpeza. |
| RNFG4 | Identificar,   com sensores, o fim da placa solar e executar uma parada.                                                                                                               |
| RNFG5 | Possuir   um dispositivo de injeção de sabão para limpezas mais pesadas                                                                                                                 |

## **Requisitos Específicos**

A partir dos requisitos gerais, cada área, responsável pelo desenvolvimento dos subsistemas do produto, identificou os requisitos específicos àquele subsistema. Estes requisitos são essenciais para o desenvolvimento da arquitetura de cada subsistema.

Abaixo apresenta-se os requisitos funcionais e não funcionais de cada área.

### **Requisitos de Estruturas**

**RFE - Requisitos Funcionais de Estruturas** <br>
**RNFE - Requisitos Não Funcionais de Estruturas** <br><br>

**Tabela 4: Requisitos funcionais de estruturas**

| **ID** | **Descrição**                                                                                                                       |
|--------|-------------------------------------------------------------------------------------------------------------------------------------|
| RFE1   | O sistema deve se mover no sentido do eixo lateral das placas, e ser fixo nos outros eixos                                          |
| RFE2   | O sistema deve aguentar seu peso próprio, incluindo os componentes eletrônicos, de alimentação e de limpeza acoplados               |
| RFE3   | A estrutura deve resistir aos esforços que ocorrem devido à rotação dos elementos limpadores                                        |
| RFE4   | O sistema deve garantir tração o suficiente para movimentação do limpador mesmo quando as superfícies de contato estiverem molhadas |
| RFE5   | A estrutura deve garantir pontos de fixação para os outros subsistemas                                                              |
| RFE6   | A estrutura deve garantir vedação para proteger subsistemas que não possam entrar em contato com água                               |


**Tabela 5: Requisitos não funcionais de estruturas**

| **ID** | **Descrição**                                                                                                        |
|--------|----------------------------------------------------------------------------------------------------------------------|
| RNFE1  | A estrutura deve poder ser instalada sobre as placas sem necessidade de desmontá-la e por no máximo duas pessoas                                                             |
| RNFE2  | A estrutura deve permitir a sua manutenção                                                                           |
| RNFE3  | A estrutura deve ser feita de materiais resistentes à corrosão, ou ter tratamento nas peças susceptíveis de corrosão |
| RNFE4  | A massa da estrutura deve ser limitada para evitar causar danos às placas                                            |

### **Requisitos de Energia e Eletrônica**

**RFEE - Requisitos Funcionais de Energia e Eletrônica** <br>
**RNFEE - Requisitos Não Funcionais de Energia e Eletrônica** <br><br>

**Tabela 6: Requisitos funcionais de energia e eletrônica**

| **ID** | **Descrição**                                                                                                                                           |
|--------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| RFEE1  | O limpador deve ser alimentado por uma fonte constante de energia elétrica compatível com seu sistema                                                   |
| RFEE2  | O sistema deve ser projetado pensando na sua manutenção de forma a permitir a rápida inspeção, reparo ou substituição de componentes elétricos |
| RFEE3  | Deve incluir sistemas de monitoramento para alertar os operadores sobre falhas ou problemas elétricos                                                   |

**Tabela 7: Requisitos não funcionais de energia e eletrônica**

| **ID** | **Descrição**                                                                                     |
|--------|---------------------------------------------------------------------------------------------------|
| RNFEE1 | O sistema deve ser projetado e instalado de acordo com as normas de segurança elétrica aplicáveis |
| RNFEE2 | Deve incluir proteções contra sobrecarga, curto-circuito e outros riscos elétricos                |
| RNFEE3 | Deve ser projetado para minimizar o consumo de energia elétrica durante suas operações            |

### **Requisitos de Software**
**RFS - Requisitos Funcionais de Software** <br>
**RNFS - Requisitos Não Funcionais de Software** <br><br>

**Tabela 8: Requisitos funcionais de software** 


| ID       | Descrição                                                                                     |
|----------|-----------------------------------------------------------------------------------------------|
| RFS01   | O sistema deve permitir a ativação manual do ciclo de limpeza por meio de um botão físico presente no dispositivo. |
| RFS02   | O sistema deve ser capaz de controlar um motor de passo para deslocar o mecanismo de limpeza ao longo do eixo da placa solar, por meio do controle das rodas. |
| RFS03   | O sistema deve ser capaz de controlar um motor de passo para girar, em seu eixo, os tubos limpadores que entram em contato com a superfície da placa. |
| RFS04   | O sistema deve ser capaz de comandar uma válvula solenoide para iniciar o fluxo de água, controlando a vazão conforme necessário durante o processo de limpeza. |
| RFS05   | O sistema deve ser capaz de comandar uma válvula solenoide para interromper o fluxo de água, após certo período de tempo. |
| RFS06   | O sistema deve incorporar um sensor de fim de curso, para detectar o término do percurso do mecanismo de limpeza na placa solar. |
| RFS07   | O sistema deve sinalizar o fim do processo de limpeza, por meio da detecção do sensor. |
| RFS07   | O sistema deve ser capaz de transmitir dados de operação, incluindo tempo de ciclo de limpeza, tempo em que a válvula ficou aberta e data e hora da operação. |
| RFS08   | O sistema deve implementar medidas de segurança para interromper a operação caso detecte obstrução ou funcionamento anormal dos motores ou da válvula. |
| RFS09   | O aplicativo deve incluir opções de cadastrar, editar e excluir limpadores. |
| RFS10   | O aplicativo deve incluir a opção de ligar todos os limpadores ao mesmo tempo. |
| RFS11   | O aplicativo deve permitir a escolha da limpeza para um limpador em específico. |
| RFS12   | O aplicativo deve mostrar o tempo para o fim da limpeza (geral ou específica). |
| RFS13   | O aplicativo deve permitir ligar a limpeza para um limpador específico. |

**Tabela 9: Requisitos não funcionais de software** 

| ID       | Descrição                                                                                     |
|----------|-----------------------------------------------------------------------------------------------|
| RNFS01  | O sistema deve ter alta disponibilidade e resistência, com capacidade de operar em ambiente externo e condições climáticas variadas. |
| RNFS02  | O sistema deve garantir a segurança nas operações, com mecanismos de proteção para evitar danos às placas solares durante a limpeza. |
| RNFS03  | O sistema deve ser projetado para eficiência energética, otimizando o consumo de energia dos motores de passo e válvula solenoide. |
| RNFS04  | O sistema deve ter um tempo de resposta para iniciar o ciclo de limpeza inferior a 5 segundos após o acionamento do botão. |
| RNFS05  | O sistema deve ser robusto, projetado para operação de longo prazo com manutenção mínima requerida. |
| RNFS06  | O sistema deve ser projetado para minimizar o uso de água, otimizando a vazão necessária para a limpeza eficiente. |
| RNFS07  | O software deve ser capaz de operar de forma contínua. |
| RNFS08  | O software deve ser desenvolvido usando o ambiente de programação embarcado de EPS32. |
| RNFS09  | O aplicativo deve funcionar em dispositivos Android. |
| RNFS10  | O aplicativo deve persistir os dados do usuário no próprio dispositivo. |