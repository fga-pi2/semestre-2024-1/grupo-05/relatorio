
# **Arquitetura do subsistema estrutural**

A estrutura proposta consiste em um conjunto de três chapas metálicas horizontais nas quais estão acoplados os demais subsistemas físicos, estas são associadas por vigas com perfil vslot. Essa estrutura é apoiada na superfície e na lateral da placa solares por meio de rodas não tracionadas, restringindo seu movimento à 1 grau de liberdade.

1. Chapas metálicas <br>
    Chapas dimensionadas para conter os demais subsistemas e proporcionar maior rigidez estrutural.
2. Rodas  
    Rodas de borracha dimensionadas à fim de evitar deslizamentos e melhor distribuição do peso da estrutura.
3. Alocamento de subsistemas  
    O subsistema eletrônico é posicionado na chapa superior, enquanto o mecânico é dividido entre a chapa superior e a central, e por fim o subsistema elétrico é, por sua maioria, posicionado na placa inferior.

**Figura 6: Arquitetura de estruturas**

![arquitetura_estrut](./assets/images/arq-Estrutural.png)