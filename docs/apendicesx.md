# **Apêndice 09 - Manual de montagem e uso do produto**

Esta seção apresenta o manual de uso do produto. De forma opcional, este
manual pode ser entregue em arquivo separado.

# **Apêndice 10 - Manual de manutenção do produto**

Esta seção apresenta o manual de uso do produto. De forma opcional, este
manual pode ser entregue em arquivo separado.

# **Apêndice 11 - Testes e Manual de instalação do software**

Esta seção apresenta a estratégia realizada para efetivação dos testes
de software(unitário, integração, funcional, aceitação, desempenho,
carga, vulnerabilidade ou outros) manual de instalação e uso dos
produtos de software. De forma opcional, o manual de uso pode ser
entregue em arquivo separado.

# **Apêndice 12 - Autoavaliação dos integrantes**

Em todos os pontos de controle, todos os integrantes de um grupo devem
realizar uma autoavaliação e anexá-la ao relatório entregue pelo grupo.
Esta autoavaliação consistirá em uma tabela com os nomes dos alunos do
grupo e uma descrição de como cada um deles contribuiu individualmente
para o projeto.

