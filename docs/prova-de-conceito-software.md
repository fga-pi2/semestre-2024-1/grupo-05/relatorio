# **Projeto do subsistema de Software**

## Guia de Estilo

<p>Conforme definido na arquitetura do projeto, estamos desenvolvendo um aplicativo de celular para fornecer uma interface do sistema ao usuário final. Para garantir uma experiência do usuário de qualidade, é crucial estabelecer características importantes desta interface por meio de um guia de estilo que deve ser seguido.</p>

<p>Um guia de estilo engloba decisões de design que envolvem os principais elementos e considerações de interface. Marcus (1991) identifica os seguintes elementos-chave:</p>

- layout
- tipografia
- simbolismo
- cores

### Layout

<p>O layout define a organização dos elementos na tela, influenciando diretamente na percepção e usabilidade do aplicativo. O Material Design, referência em design mobile, propõe uma estrutura composta por três elementos principais:</p>

1. Colunas (Rosa): Áreas onde o conteúdo principal da tela será exibido.

2. "Gutters" ou Espaços Vazios (Azul): Espaçamentos estratégicos entre as colunas, que servem para separar o conteúdo e facilitar a leitura.

3. Margens (Verde): Espaço entre as bordas da tela e os elementos, proporcionando respiro visual e evitando a sensação de sobrecarga.

**Figura 39: Estrutura de layout para mobile**

![Estrutura de layout para mobile](./assets/images/mobile_layout.png)

<p align="center">Fonte: https://m2.material.io/design/layout/responsive-layout-grid.html#columns-gutters-and-margins.</p>

<p>Essa estrutura básica pode ser customizada de acordo com as necessidades da aplicação, sempre buscando o equilíbrio entre funcionalidade e estética.</p>

<p>Componentes do Layout: Header e Body</p>

1. Header: 
    - Localizado na parte superior da tela, serve como referência para o usuário, facilitando a navegação entre as diferentes seções do aplicativo.

2. Body:
    - Ocupa a maior parte da tela e é responsável por apresentar o conteúdo principal da aplicação e suas funcionalidades.
    - Deve ser organizado de forma intuitiva, utilizando elementos visuais que guiem o usuário na jornada dentro do aplicativo.

### Tipografia

<p>A escolha da tipografia desempenha um papel fundamental no design de interfaces de usuário, influenciando diretamente a legibilidade, a estética visual e a experiência geral do usuário. Para este aplicativo, optamos por utilizar uma fonte sem serifa (sans-serif) devido aos seguintes argumentos de UI/UX:</p>

1. Legibilidade em Dispositivos Móveis:
    Fontes sem serifa tendem a ser mais legíveis em telas de dispositivos móveis, especialmente em tamanhos menores. A ausência de traços adicionais nas serifas ajuda a manter a clareza das letras em tamanhos reduzidos, garantindo uma melhor experiência de leitura para os usuários.

2.    Estilo Moderno e Limpo:
    Fontes sem serifa são frequentemente associadas a um estilo moderno e limpo, o que pode refletir uma abordagem contemporânea no design do aplicativo. Essa escolha tipográfica transmite uma sensação de simplicidade e elegância, alinhando-se com uma estética minimalista desejada para a interface.

3.    Consistência e Uniformidade:
    Utilizar uma única fonte sem serifa em diálogos, formulários e relatórios contribui para a consistência visual em todo o aplicativo. Isso ajuda a criar uma experiência coesa e uniforme para os usuários, facilitando a compreensão e a navegação.

4.    Adaptação a Diferentes Resoluções:
    Fontes sem serifa são mais versáteis e adaptáveis a diferentes resoluções de tela, mantendo sua clareza e legibilidade em dispositivos de diferentes tamanhos e densidades de pixels.

<p>Para este guia de estilo, recomendamos o uso da fonte Roboto como nossa escolha principal de tipografia sem serifa. O Roboto é uma fonte amplamente reconhecida no design de interfaces, desenvolvida pelo Google, que oferece excelente legibilidade em dispositivos móveis e uma aparência moderna e amigável. Sua variedade de pesos e estilos também permite uma aplicação flexível em diferentes contextos dentro do aplicativo, garantindo uma experiência de usuário consistente e agradável.</p>

### Simbolismo

<p>De acordo com o "Guia de Estilo do Portal Institucional Padrão", o uso de pictogramas auxilia na compreensão do conteúdo, sendo fundamental considerar os casos ideais para sua utilização. É relevante ressaltar que, para situações em que não existam ícones pré-estabelecidos, é possível criar novos ícones utilizando no máximo duas cores, alinhadas com a identidade visual definida neste Guia de Estilo.</p>

<p>Recomenda-se utilizar o site [flaticon.com](https://www.flaticon.com/). para buscar ícones adequados. O Flaticon oferece uma biblioteca de ícones que podem ser personalizados de acordo com as cores especificadas no guia, garantindo consistência visual e facilitando a integração dos pictogramas ao design da interface.</p>

### Cores

<p>A escolha das cores para a identidade visual de uma aplicação de limpeza de placas solares é crucial para transmitir os valores e a funcionalidade do serviço de forma eficaz. As cores selecionadas são fundamentais para criar uma experiência visual atraente e funcional para os usuários, considerando aspectos de UI/UX.
Razões da Escolha das Cores:</p>

1. Cor Primária (#3bb2f8):

    **Associada à Tecnologia**: O azul claro (#3bb2f8) é frequentemente utilizado em aplicativos relacionados à tecnologia e serviços profissionais. Essa cor transmite confiança, inovação e eficiência, refletindo a natureza avançada e moderna da aplicação de limpeza de placas solares.

2. Cor Secundária (#c2fb74):

    **Referência à Natureza e Sustentabilidade**: O verde claro (#c2fb74) evoca sentimentos de natureza, frescor e sustentabilidade. Essa cor é ideal para uma aplicação focada em energias renováveis e limpeza ambiental, associando-se diretamente ao contexto das placas solares.

3. Cores de desfalque Primária e Secundária (#94d3fa e #dbfbb4):

    **Destaque e Acentuação**: O uso do tom de azul mais suave (#94d3fa) e do tom de verde musgo (#dbfbb4) é empregado para desfocar ou atenuar elementos menos importantes da interface quando o usuário está realizando uma tarefa específica. Essas cores são utilizadas estrategicamente para garantir que o foco visual seja direcionado para a atividade principal em execução, proporcionando uma experiência mais fluida e concentrada para o usuário.


## Subsistema Aplicativo

### Escolha da Tecnologia - Flutter

A escolha do Flutter para o desenvolvimento do aplicativo do projeto é fundamentada em uma série de benefícios específicos em comparação com outras tecnologias, como React Native e desenvolvimento nativo (iOS e Android). (RELEVANT SOFTWARE, 2024)

#### Benefícios do Flutter

1. **Desenvolvimento Rápido e Eficiente**
    * **Hot Reload**: que permite aos desenvolvedores ver as mudanças instantaneamente sem perder o estado da aplicação, o que acelera o ciclo de desenvolvimento e debugging.
2. **Desempenho Quase nativo**
    * **Motor de Renderização**: usa o motor de renderização Skia, que é altamente eficiente e permite desempenho quase nativo.
    * **Compilação Ahead-of-Time (AOT)***: Compila o código Dart diretamente para código de máquina nativo, eliminando a necessidade de uma ponte (bridge) entre o código da aplicação e o código nativo.
3. **Plataforma Unificada**
    * **Código Único para Múltiplas Plataformas**: pode escrever um único código-base para aplicativos iOS e Android, economizando tempo e recursos.
4. **Comunidade Ativa**
    * **Alto Market Share**: 42% dos desenvolvedores mobile usam Flutter, o que faz se uso ser altamente difuso e, portanto, com muita documentação disponível.

#### Comparação com outras tecnologias

1. **React Native**: Apesar de ter uma grande comunidade e oferecer Hot Reload também, o React Native tem um gargalho de desempenho, já que ele depende de uma ponte (bridge) entre o JavaScript e o código nativo, o que pode causar problemas de desempenho em aplicativos mais complexos.

2. **Desenvolvimento Nativo (iOS e Android)**: natualmente, o desempenho é o melhor possível, pois o aplicativo já é feito diretamente para o sistema operacional. Contudo, o desenvolvimento é mais complexo, já que será necessário a construção de dois códigos-fontes (um para cada sistema operacional).

### Subsistema - Banco de dados

#### Escolha da Tecnologia - SQLite

A escolha do SQLite para a comunicação com Flutter e armazenamento de dados no dispositivo móvel de um aplicativo é baseada em uma série de benefícios específicos em comparação com outras tecnologias, como Hive e outros bancos de dados móveis como Realm e Firebase Realtime Database. (SQL EASY, 2024)

**Benefícios SQLite**:

1. **Leveza e portabilidade**
    * **Tamanho Reduzido**: SQLite é um banco de dados muito leve, com um binário que pode ser menor que 600 KB, tornando-o ideal para dispositivos móveis.
    * **Portabilidade**: não depende de um servidor externo ou de um processo de configuração complexo. Isso facilita a implantação em qualquer dispositivo móvel.
2. **Desempenho**
    * **Velocidade**: é conhecido por sua alta velocidade em operações de leitura e escrita, o que é essencial para aplicativos que precisam ser responsivos.
3. **Integração com Flutter**
    * **Plugins**: Flutter possui plugins como sqflite que facilitam a integração do SQLite, oferecendo APIs simples e bem documentadas para realizar operações de banco de dados.

**Comparação com outras tecnologias**:

1. **Hive**: é um banco NoSQL leve e rápido, otimizado para Flutter com ótimo desempenho para operações de CRUD. Contudo, não suporta SQL, o que é essencial para dados estruturados usados pelo projeto.
2. **Realm**: oferece desempenho alto para leituras e escritas, mas é complexo na integração com o Flutter em comparação com o SQLite.
3. **Firebase Realtime Database**: é ideal para sincronização de dados em tempo real entre dispositivos e plataformas, porém necessita de conexão constante com a internet, o que limita seu uso na proposta do aplicativo.

### Prototipação de Média Fidelidade do Aplicativo

**Figura 40: Interface de usuário do aplicativo**

![alt text](./assets/images/prototipo_media_fidelidade3.png)
![alt text](./assets/images/prototipo_media_fidelidade1.png)
![alt text](./assets/images/prototipo_media_fidelidade2.png)

## Subsistema Eletrônico (ESP)

### Escolha da Tecnologia - ESP-IDF

A escolha do ESP-IDF (Espressif IoT Development Framework) para programar controladores, especialmente usando placas ESP32, é justificada por uma série de benefícios específicos em comparação com outras tecnologias, como Arduino IDE.

#### Benefícios do ESP-IDF

1. **Controle Total e Flexibilidade**
    * **Acesso Completo a ESP**: oferece APIs detalhadas que permitem um controle granular sobre todos os aspectos do hardware ESP, incluindo GPIO, Wi-Fi, Bluetooth etc.
    * **RTOS**: permitindo a criação de aplicações complexas com multitarefa e gerenciamento avançado de tempo real.
2. **Desempenho e Eficiência**
    * **Otimização de Recursos**: permite otimizações de desempenho e uso eficiente dos recursos do hardware, essencial para aplicações comerciais e industriais que exigem alta eficiência.
    * **Configuração Detalhada**: possui ferramentas de configuração detalhada, como o "menuconfig", para ajustes finos de parâmetros do sistema.

#### Comparação com outras tecnologias

1. **Arduino IDE**: apesar de ser extremamente fácil de usar e configurar e, consequentemente, melhor para projetos não muito complexos, o Arduino IDE possui limitações, já que não oferece o mesmo nível de controle, otimização e funcionalidades avançadas que o ESP-IDF. Com isso, ele é ideal para prototipagem rápida, mas pode ser insuficiente para projetos mais complexos.