# Plano de Testes de Software

## 1. Introdução

### 1.1. Escopo
Este documento serve como um guia abrangente que delineia a abordagem, objetivos e metodologias para testar o sistema, abordando testes unitários, de integração e aceitação.

### 1.2. Visão Geral do Sistema e Principais Recursos
O sistema embarcado para limpeza de painéis solares é projetado para automatizar o processo de limpeza, utilizando motores controlados pela placa ESP32 e a integração da ESP com um aplicativo móvel.

### 1.3. Visão Geral de Testes
Serão realizados testes unitários e de integração no aplicativo móvel, e também testes de aceitação, que serão realizados junto com os professores da disciplina simulando clientes.

#### 1.3.1 Organização
A relação dos processos de teste com outros processos, como desenvolvimento, gerenciamento de projetos, será feita através das issues do GitLab. Onde serão criadas issues específicas para o desenvolvimento de testes de uma determinada funcionalidade.

#### 1.3.2 Ferramentas
- Flutter Test Framework: Pacote de testes do flutter
- Mockito: Biblioteca para mocks

## 2. Processo de Testes

### 2.1 Testes Unitários

- Teste do serviço MQTT do aplicativo;
- Testes dos CRUDs e funcionalidades dos limpadores.

### 2.1.1 Testes de Integração

- Teste do fluxo de enviar configuração do aplicativo para a ESP32;
- Teste de comunicação entre ESP32 e aplicativo acerca da limpeza.

### 2.1.2 Teste de Aceitação

- Utilização do sistema junto aos professores;
- Simulação completa do fluxo desenhado acerca dos diferentes tipos de limpeza.

### 2.2 Requisitos de documentação dos testes
- Deve ser descrito na Issue onde o teste foi feito quais foram os resultados obtidos.
- Deve ser realizada uma matriz de rastreabilidade relacionando os testes criados ao requisito que gerou os testes no caso de testes unitários e de integração.

## 3. Geral
### 3.1. Glossário

**Glossário**

**ESP32:** Placa de desenvolvimento de hardware de código aberto baseada no chip ESP32 da Espressif Systems, utilizada para controle de dispositivos e integração em sistemas embarcados.

**Testes Unitários:** Processo de teste no qual unidades individuais ou componentes do software são testados para garantir que cada unidade funcione corretamente de forma isolada.

**Testes de Integração:** Processo de teste no qual diferentes unidades ou componentes de software são combinados e testados como um grupo para garantir que funcionem juntos de forma adequada.

**Testes de Aceitação:** Processo de teste no qual o software é testado para garantir que atende aos critérios de aceitação definidos pelos usuários finais ou clientes.

**MQTT (Message Queuing Telemetry Transport):** Protocolo de mensagens leve e assíncrono projetado para situações em que a rede pode ser intermitente ou a largura de banda é limitada, comumente usado em aplicações IoT para troca de mensagens entre dispositivos.

**Issues do GitLab:** Recurso do GitLab para rastrear e gerenciar problemas, melhorias e tarefas relacionadas ao desenvolvimento de software.