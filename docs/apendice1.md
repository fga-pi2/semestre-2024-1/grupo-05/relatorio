
 
# **Apêndice 01 - Aspectos de gerenciamento do projeto**

## **Termo de abertura do projeto**

### **Justificativa**

Desenvolver uma solução automática de fácil instalação, e que requeira o mínimo de intervenção do operador durante o seu funcionamento, para limpeza de painéis solares em diversos tipos de instalações, desde residenciais até escala industrial.


### **Objetivos**

Tem-se neste projeto o objetivo de conceber e construir um protótipo do limpador de placas solares que se movimente sobre a placa par limpá-la de forma completa, bem como elaborar o seu sistema de controle de forma que um operador possa operar o limpador de forma simples e ágil.

### ***Stakeholders***

#### **Equipe do Projeto**

##### Papel e Responsabilidade

Composta por 15 alunos dos diversos cursos de engenharia da Faculdade do Gama, a equipe do projeto é responsável pela sua elaboração, devendo identificar um problema, propor e desenvolver uma solução de engenharia para este problema. Esta solução deve contemplas subsistemas a serem desenvolvidos por cada equipe técnica de desenvolvimento em função de seu curso. Durante o decorrer do projeto estes subsistemas deverão ser integrados de forma a culminar na construção de um protótipo funcional da solução proposta.

##### Expectativas

Espera-se de cada membro da equipe que colabore na produção do projeto de forma eficaz, realizando entregas segundo o cronograma elaborado pela gestão da equipe, de acordo com a suas responsabilidades segundo sua ocupação na estrutura da equipe. É crucial que todos os integrantes da equipe estejam sempre em comunicação com sua respectiva gestão.

#### **Professores**

##### Papel e Responsabilidade

Os professores da disciplina tem como papel auxiliar os alunos no desenvolvimento do projeto por meio de orientação e auxílio durante o seu desenvolvimento.

##### Expectativas

Sua expectativa é que a equipe entregue pontualmente todos os pontos de controle, havendo realizado um trabalho de qualidade, e que ao final do projeto a equipe tenha cumprido com todos os requisitos da disciplina de PI2 com a montagem de um protótipo funcional do projeto e a documentação a ele referente.


### **Gestor do projeto e sua autoridade**

O aluno Gabriel Barbosa Pfeilsticker de Knegt, matrícula 180101056, foi designado coordenador geral do projeto.

Este tem a autoridade exclusiva de solicitar a contribuição monetária dos membros da equipe afim de obter recursos para o projeto, bem como autorizar gastos provenientes destes recursos, sendo assim o único responsável pelo controle do fluxo de caixa da equipe. Com objetivo de transparência, este controle feito pelo coordenador geral ficará disponível para visualização para todos os membros da equipe.

Uma vez fixados os objetivos gerais, específicos, requisitos de projeto, arquitetura geral do projeto, arquitetura de subsistemas e cronograma macro de marcos, apenas este tem autoridade de autorizar alterações significativas a estes.

Este divide sua autoridade com os diretores técnicos no que tange à atribuição de tarefas aos desenvolvedores e à definição do cronograma específico de entregáveis de cada área, desde que este cronograma respeite o cronograma macro de marcos do projeto.


### **Premissas Organizacionais**


#### **Recursos Financeiros**

O projeto conta com disponibilidade de **R$2.250,00** (dois mil duzentos e cinquenta reais), oriundos da contribuição de **R$150,00** (cento e cinquenta reais) por cada membro da equipe, para cobrir todas as despesas durante o desenvolvimento do projeto.

#### **Recursos Humanos**

A equipe multidisciplinar será dividida segundo suas áreas de conhecimento, sendo estruturada em três níveis organizacionais: coordenação e gestão da qualidade (2 colaboradores), direção técnica (3 colaboradores), e desenvolvedores (10 colaboradores).

Os desenvolvedores colaborarão de forma ativa, estando em constante comunicação com sua respectiva direção técnica. Os diretores técnicos estarão em constante comunicação com o coordenador geral e gestor de qualidade. Assim o projeto será elaborado de forma eficiente e coesa.

#### **Recursos Físicos**

A equipe conta com a disponibilidade dos laboratórios da Faculdade do Gama para elaboração do seu projeto. A utilização destes deverá ser coordenada com os respectivos professores da disciplina.


### **Restrições**


#### **Cronograma**

O projeto deve ser concluído no decorrer do semestre letivo vigente, sendo respeitadas as fases a serem concluídas em cada ponto de controle.

#### **Alterações do escopo**

Uma vez definido o escopo do projeto e as arquiteturas de projeto, deve-se evitar a realização de alterações significativas, uma vez que estas alterações podem dificultar ou inviabilizar a integração dos subsistemas. Em caso de necessidade irremediável de alterações de grandes proporções, uma reunião com todos os gestores (coordenador geral, gestor de qualidade e diretores técnicos) deve ser convocada para minimizar os impactos desta mudança.

#### **Orçamento**

O projeto deve ser concluído com o valor discriminado anteriormente. Eventuais despesas adicionais devidas a imprevistos deverão ser discutidas com toda a equipe a aprovadas pelo coordenador geral.

### **Riscos**

#### **Trancamento de matrícula**

Caso algum integrante da equipe tenha de realizar o trancamento da disciplina ou do semestre letivo este deverá informar o quanto antes o coordenador geral para que suas tarefas sejam redistribuídas na equipe.

#### **Problemas de integração**

Apesar de todas as áreas coordenarem seus esforços durante a elaboração do projeto, existe o risco da ocorrência de problemas no momento da integração dos subsistemas desenvolvidos. Comunicação constante e a elaboração de um plano de integração robusto podem mitigar este risco. Este risco é potencializado pela ocorrência de erros no projeto de algum subsistema. 

#### **Greve e suspenção do semestre**

Devido à atual situação da Universidade de Brasília, com greve simultânea de técnicos e professores, existe o risco de suspenção do semestre letivo, tendo um impacto no cronograma do projeto, porém não impossibilitando a sua conclusão. Até que haja posição oficial da reitoria da universidade, os trabalhos devem seguir normalmente. 

#### **Vedação da parte elétrica e eletrônica**

Como o limpador operará com água, é imperativo que a equipe se atente a realizar a proteção adequada da parte elétrica e eletrônica do projeto para que esta possa operar sem causar riscos aos operador e sem ser danificada pela água. Uma falha nesta proteção pode inviabilizar a conclusão do projeto.


### **Subprodutos**

**Tabela 14: Subprodutos e sua descrição.**

| **Subproduto**              | **Descrição**                                                                                                                                                   |
|-----------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Estrutura do limpador       | Estrutura básica que confere a forma e integridade ao limpador. É nesta estrutura que serão fixados os outros componentes.                                      |
| Sistema de limpeza          | Composto por um aspersor de água e cerdas rotativas que têm por objetivo a retirada de sujeira das placas.                                                      |
| Mecanismo para movimentação | Mecanismo que permite que a estrutura de desloque como um todo sobre a placa para limpá-la por completo.                                                        |
| Sistema de alimentação      | Conjunto responsável por garantir a alimentação em energia elétrica para o funcionamento do produto.                                                            |
| Módulo de controle          | Módulo que permite atuar o sistema de limpeza e o mecanismo de movimentação em conjunto segundo desejos do operador e necessidades da operação a ser realizada. |
| Interface de usuário        | Interface pela qual o usuário realizara o controle do limpador, dando os comandos desejados.                                                                    |
| Documentação                | Documentação que abrange os manuais de montagem, operação, manutenção e informações técnicas sobre o limpador.                                                  |


### ***Milestones***


**Tabela 15: Cronograma geral com marcos principais do projeto.**

![Cronograma do projeto](assets/images/milestones.png)


## **Lista É / Não É**

Relação do que o produto, ou subproduto, é, e do que o produto, ou
subproduto, não é.

Este processo é necessário para restringir ao seu mínimo o escopo do
projeto, garantindo um melhor foco.

**Tabela 16: Lista é / não é.**

| **É** | **Não É** |
| ------ | ------ |
|    Um sistema automático de limpeza de placas solares.    |    Um sistema autônomo de limpeza de placas solares.    |
|    Um limpador que se move em um único eixo.    |    Um limpador que se desloca em múltiplos eixos    |
| Um dispositivo que necessita de fornecimento de água. | Um dispositivo com reservatório próprio de água. |
| Um projeto que pode ser alterado para ser utilizado em placas de diferentes tamanhos. | Um aparelho que se adapta a placas de tamanhos diferentes após ter sido construído.|

## **Organização da equipe**

**Figura 42: Organograma da equipe de desenvolvimento**

![Organograma da equipe](assets/images/Equipe_PI2.png)


## **Repositórios**

Os repositórios do projeto são:

- [Relatório](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-05/relatorio)
- [Estruturas](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-05/estruturas)
- [Eletrônica](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-05/eletronica)
- [Energia](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-05/energia)
- [Software](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-05/software)

## **EAP (Estrutura Analítica de Projeto) Geral do Projeto**

**Figura 43: EAP geral do projeto.**

![EAP geral](assets/images/EAP_geral.png)

### **EAP de Estruturas**

**Figura 44: EAP de estruturas.**

![EAP estruturas](assets/images/EAP_estruturas.png)


### **EAP de Energia e Eletrônica**

**Figura 45: EAP de energia e eletrônica.**

![eap energia](assets/images/eap_energia.png)

#### **EAP Software**

**Figura 46: EAP de software.**

![EAP Software](assets/images/EAP_Software.png)

### **Definição de atividades e backlog das sprints**

Definir as atividades gerais que compõe o projeto e elaborar o
cronograma básico de execução. É importante identificar os responsáveis
pelas atividades.

## **Estimativa de custos e orçamento**

Foi realizado o levantamento da necessidade em material do projeto. Após pesquisa e solicitação de orçamento, as melhores opções foram compiladas em uma tabela para formar a lista de compras preliminar da equipe, possibilitando assim a estimativa de custos do projeto.

**Tabela 17: Lista de compras de materiais**

![Orçamento](assets/images/Orçamento.png)
