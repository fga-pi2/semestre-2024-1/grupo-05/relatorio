# **Arquitetura do subsistema elétrico**
 
O esquema apresentado abaixo ilustra a arquitetura do subsistema elétrico.

**Figura 9: Esquema da arquitetura do subsistema elétrico**

![identificador](assets/images/arq-Energia.png)

O objetivo deste subsistema é o fornecimento de alimentação elétrica para todos os outros subsistemas do limpador. Segundo os requisitos do projeto, o subsistema elétrico deverá garantir esse fornecimento elétrico utilizando uma bateria, cujo dimensionamento mantenha em funcionamento todos os componentes dos outros subsistemas por um período de duas horas.

É imprescindível também garantir o bom isolamento deste subsistema, uma vez que a estrutura do limpador será metálica e terá contato com água, potencializando o risco de choques elétricos. É necessário que haja alguma forma de desenergizar o equipamento para transporte e manutenção deste. Assim um interruptor deve ser capaz de isolar completamente a bateria dos demais componentes elétricos e eletrônicos do limpador.


