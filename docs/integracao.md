# **Integração de subsistemas e finalização do produto**

<!-- A seção 04 "**Integração de subsistemas e finalização do produto**" se refere à fase 04 do ciclo de vida do projeto e tem por objetivo detalhar a integração final dos subsistemas e a finalização do projeto de um
protótipo de produto. Dentre os aspectos a ser abordado nesta seção,
destaca-se:

-   Desenvolvimento de diagramas de integração do produto;

-   Detalhamentos de ações para integração entre subsistemas -->


# **Diagrama de integração**

<!-- Elaborar o diagrama de integração entre subsistemas, conforme modelo
apresentado [no]{.underline} Plano de Ensino. -->

**Figura 41: Diagrama de integração simplificado**

![diag_integ_CW1](assets/images/diag_integ_CW1.png)

# **Integração dos sistemas estruturais**

O sistema estrutural é composto por três subsistemas:

- Subsistema estrutural: composto pela estrutura principal cujo papel é comportar todos os outros subsistemas do produto;
- Subsistema mecânico: responsável pela movimentação da estrutura sobre a placa;
- Subsistema de limpeza: composto pelos limpadores e _dispenser_ de água.

Assim, existe neste sistema a necessidade da integração entre os subsistemas.

Para garantir a facilidade da integração entre os subsistemas e a adaptabilidade do produto a painéis solares de diferentes dimensões, a estrutura foi projetada de forma a permitir ajustes rápidos de seus componentes e o alinhamento entre as diversas seções que o compõe. 

## **Detalhamento de atividades para integração entre os subsistemas de estruturas e mecânico**

A integração entre o subsistema da estrutura e o subsistema mecânico consiste em garantir o deslocamento da estrutura sobre a placa de forma que todas as rodas estejam apoiadas sobre a estrutura e corretamente alinhadas com o sentido de deslocamento do limpador. Deve-se garantir também o tensionamento adequado da correia que liga as rodas tratoras ao motor. Para tanto, a estrutura foi fabricada com a furação para passagem de fixadores com diâmetro maior que o fixador, permitindo uma pequena movimentação destes antes de seu aperto final, possibilitando o alinhamento lateral e longitudinal dos componentes da estrutura.

Sequencia das atividades:

 1. Montagem da estrutura (esta etapa requer a conclusão da compra e fabricação de todos os componentes estruturais e mecânicos);

 2. Alinhamento dos componentes da estrutura;

 3. **Teste 1:** acoplamento da estrutura sobre o painel. Neste teste a estrutura é colocada sobre o painel com o objetivo de verificar o bom alinhamento da estrutura e garantir o deslocamento do limpador;

 4. Acoplamento sistema de transmissão, composto pelas polias nos eixos das rodas tratoras e do motor, correia dentada e tensionador, na estrutura;

 5. **Teste 2:** teste de deslocamento com sistema de transmissão. Tem como objetivo permitir o ajuste do tensionamento da correia. A depender do andamento das outras áreas este teste pode ser realizado juntamente com a integração com os subsistemas de eletrônica.


## **Detalhamento de atividades para integração entre os subsistemas de estruturas e de limpeza**

A integração entre o subsistema da estrutura e o subsistema de limpeza consiste em garantir o funcionamento do subsistema de limpeza uma vez acoplado à estrutura. Para tanto é necessária a fixação de ambos os limpadores na estrutura, e realizar o teste com o sistema hidráulico. Esta integração deve ser realizada após a integração do sistema mecânico já que é mais fácil de ser adaptada à estrutura, por ser compostas por menos peças.

Sequencia das atividades:

 1. Fixação dos rolamentos e limpadores nos eixos de limpeza;

 2. Fixação dos eixos de limpeza na estrutura;

 3. **Teste 1:** rotação dos limpadores. Este tem por objetivo garantir a boa rotação dos eixos de limpeza;

 4. Acoplamento sistema de transmissão aos eixos de limpeza;

 5. **Teste 2:** teste do sistema de limpeza. Tem como objetivo permitir o ajuste do tensionamento da correia do sistema de limpeza e garantir o seu bom funcionamento acoplado à estrutura. A depender do andamento das outras áreas este teste pode ser realizado juntamente com a integração com os subsistemas de eletrônica.


# **Detalhamento de atividades para integração entre os subsistemas de energia e eletrônica**

A integração entre o subsistema de energia e o subsistema eletrônico tem por objetivo garantir o bom funcionamento do sistema eletrônico. Para tanto a corrente e tensão fornecidos pela bateria devem estar de acordo com o dimensionamento realizado para o sistema eletrônico, sendo capaz de fornecer alimentação ininterrupta durante o tempo proposto.

Sequência de atividades:

1. Verificação da conformidade da bateria ao sistema eletrônico montado;

2. **Teste 1:** primeiro acionamento. Conexão da bateria ao sistema eletrônico com o objetivo de verificar o funcionamento do sistema quando alimentado exclusivamente pela bateria;

3. **Teste 2:** teste dos sistemas de segurança. Neste teste deve-se verificar o completo isolamento do sistema eletrônico e da bateria quando o botão de emergência é acionado;

4. **Teste 3:** teste de acionamento simultâneo de todos os componentes. Acionamento simultâneo de todos os atuadores para garantir que a bateria consegue fornecer a corrente de pico solicitada pelo sistema;

5. **Teste 4:** teste de autonomia. Acionamento de todos os componentes elétricos e eletrônicos, iniciando com a bateria em carga plena, até sua exaustão, com objetivo de garantir a autonomia de funcionamento proposta em projeto.

# **Detalhamento de atividades para integração entre os subsistemas de software e eletrônica**

A integração entre o subsistema de software e o subsistema eletrônico tem por objetivo garantir a estabilidade da comunicação entre os sistemas e a acurácia do controle da movimentação da estrutura, por meio da atuação dos atuadores de acordo com o programado pela equipe de software com o aplicativo e a parte eletrônica.

Sequência de atividades:

1. **Teste 1:** teste de conformidade. Neste teste deve-se verificar que os comandos fornecido pelos subsistemas de software têm o efeito desejado sobre os atuadores;

2. **Teste 2:** teste de comunicação. Neste teste deve-se garantir a boa comunicação dos subsistemas de software com o subsistema eletrônico de forma a possibilitar o controle do subsistema eletrônico a partir do aplicativo proposto em projeto;

3. **Teste 3:** teste dos sistemas de segurança. Neste teste deve-se garantir o funcionamento dos sensores de fim de curso, estes devem para por completo o movimento dos motores quando o sensor de fim de curso é acionado.

## Integração dos sistemas de software

O sistema de software é composto por três subsistemas:

- Subsistema do banco de dados: responsável por armazenar os dados referentes ao aplicativo e cada usuário que o utiliza;
- Subsistema do aplicativo: responsável pela comunicação com o banco de dados (ler e escrever dados dos usos dos usuários) e por enviar mensagens ao subsistema eletrônico, a fim de configurar e iniciar limpezas;
- Subsistema de eletrônica: a parte de software é responsável por programar o uso dos atuadores e transdutores.

# **Detalhamento de atividades para integração entre os subsistemas de eletrônica e sistemas estruturais**

<!-- Descrever atividades e apresentar documentação técnica referente a
integração entre os subsistemas. -->
A integração entre o subsistema eletrônico e o sistemas estruturais tem por objetivo possibilitar a movimentação da estrutura por meio dos atuadores nela acoplados. Para início desta etapa é necessário que a integração dos subsistemas que compõe a estrutura já tenha sido concluído.

Sequencia de atividades:
1. **Teste 1:** deslocamento do limpador. Este teste tem por objetivo realizar o deslocamento motorizado da estrutura sobre o painel solar;

2. **Teste 2:** funcionamento do sistema de eixos de limpeza. Este teste tem por objetivo garantir a rotação motorizada dos eixos de limpeza;

3. **Teste 3:** funcionamento do sistema hidráulico. Este teste tem por objetivo verificar o funcionamento do sistema de controle do fluxo de água do limpador.

# **Detalhamento de atividades para integração final**

<!-- Descrever atividades e apresentar documentação técnica referente a
integração entre os subsistemas. -->

A integração final tem por objetivo a conclusão da montagem final do produto. Para tanto é necessário que todas as integrações entre subsistemas tenha sido concluídas.

Sequencia das atividades:
1. Conclusão das integrações entre subsistemas;

2. Montagem de subsistemas sobre a estrutura do limpador;

3. **Teste 1:** primeiro acionamento. Neste teste realizar-se-á o primeiro acionamento dos componentes eletrônicos após conclusão da montagem da estrutura;

4. **Teste 2:** comunicação e conformidade do subsistema de software. Este teste tem por objetivo garantir a boa comunicação do sistema de software e a conformidade das instruções por ele fornecido ao sistema eletrônico antes do acoplamento da estrutura ao painel, para evitar deslocamentos excessivos ou muito rápidos que possam danificar o produto ou ferir alguém;

5. **Teste 3:** teste dos sistemas de segurança. Neste teste, ainda com a estrutura fora da placa, devem ser testados os sistemas de segurança para garantir a parada total do limpador em caso de emergência. São estes sistemas os sensores de fim de curso e o botão de emergência;

6. **Teste 4:** teste de deslocamento. Neste teste tem-se por objetivo o deslocamento motorizado e controlado pelo sistema de software do limpador sobre a placa;

7. **Teste 5:** teste do limpador. Neste teste tem-se por objetivo o acionamento motorizado e controlado pelo sistema de software da rotação dos eixos de limpeza;

8. **Teste 6:** teste do sistema hidráulico. Neste teste tem-se por objetivo o acionamento controlado pelo sistema de software do fluxo de água;

9. **Teste 7:** Teste de final de integração. Este teste tem como objetivo validar a fase de integração do projeto. Nele o limpador deve ser posicionado sobre o painel solar, um ciclo de limpeza será selecionado no aplicativo. O limpador será considerado validado se completar todos os ciclos montados no aplicativo da forma esperada e sem a necessidade de paradas manuais ou de emergência durante seu funcionamento.