# **Projeto do subsistema estrutural**

A Fig. 11 mostra o CAD renderizado da máquina limpadora de placas solares Helios CW1. A vista explodida e desenhos técnicos de todos os componentes estão disponívels no [Apêndice 2](https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-05/relatorio/apendice2/).


**Figura 11: Renderização do Helios CW1**

![render1](assets/images/render1_pc2.JPG)
![render2](assets/images/render2_pc2.JPG)

Fonte: Elaboração própria

Dentre os componentes estruturais, podem ser listados, principalmente:

1. Chapas de aço
    Foram utilizadas 3 chapas de aço carbono de 1/4" (~3.175mm) de espessura, duas nas extremidades superior e inferior da placa solar, e a outra no meio da placa. Enquanto as chapas de aço inferior e do meio (400mm x 155mm) servem apenas para fornecer apoio para os limpadores e perfis estruturais, é na chapa de aço superior (400mm x 310mm) onde serão fixados os componentes eletrônicos. Na chapa de aço superior também se encontra um par de rodas que se apoiam na lateral da placa solar a fim de permitir a operação do Helios CW1 em placas solares inclinadas em até $`20^\circ`$.

2. Perfis estruturais
    Foram utilizados 6 perfis estruturais vslot de alumínio (20mm x 20mm) de 1 metro de comprimento cada, a fim de conectar as chapas de aço umas às outras. A fixação entre os perfis estruturais e as chapas de aço é feita a partir de cantoneiras de alumínio, de maneira que a distância entre as chapas de aço possam ser facilmente ajustadas, permitindo que o Helios CW1 seja acoplado a placas solares de diversas dimensões (contanto que os limpadores apropriados estejam disponíveis).

3. Rodas
    Foram utilizadas no total 9 rodas de borracha de 4" (~10cm) de diâmetro e 28mm de largura no aparato completo, 6 para apoiar a máquina na placa solar, 2 apoiadas na lateral da placa e 1 utilizada para aplicar tensão na fixação superior da máquina. Devido ao material da roda ser feita de borracha em seu perímetro externo, a área de contato entre a roda e a placa aumenta quanto maior for a carga aplicada, aumentando também a área de contato entre ambas as superfícies e distribuindo de maneira mais efetiva o peso sobre a placa solar. As rodas também possuem um rolamento em seu perímetro interno, permitindo uma operação mais suave e livre de atrito desnecessário, facilitando ainda mais a operação dos motores de passo.

4. Eixos
    O aparato que conecta as rodas às chapas de aço é composto por um eixo de aço (ou nylon nas rodas laterais) e um apoio feito de polietileno. O segundo ponto de apoio para cada roda é feito a partir de um engaste (parafusado na chapa de aço) para as rodas com movimento livre ou um mancal KFL12 para as rodas motorizadas.

5. Tensionador
    Devido à operação do Helios CW1 em placas solares molhadas, se mostra necessário a utilização de um tensionador que possa aumentar a força normal sobre as rodas motorizadas, evitando assim que as mesmas derrapem quando aplicadas um torque. O aparato tensionador é composto por uma haste articulada de 200mm de comprimento, com uma roda em sua extremidade livre e duas molas de compressão, corforme a Fig. 12.

**Figura 12: Vista renderizada do tensionador**

![tensionador_render](assets/images/tensionador_render.JPG)


De acordo com o esquemático da Figura 13, a força $`F_3`$ que é aplicada na estrutura pela roda tem o valor de $`F_3 = \frac{F_1 x_1 + F_2 x_2}{x_3}`$, onde $`x_i`$ é a distância horizontal entre o centro de rotação e o local de aplicação da força $`F_i`$, $`i=\{1,2,3\}`$. As forças $`F_1`$ e $`F_2`$ podem ser calculadas a partir da deflexão de cada mola, isto é, para as molas que estão sendo utilizadas no projeto, cada milímetro defletido gera uma força de 1 kilograma força na direção. Adimitindo uma deflexão máxima de até 35% do comprimento inicial da mola, é possível obter uma força $`F_3`$ de até 14,56 kgf.

**Figura 13: Forças atuantes no tensionador**

![tensionador](assets/images/tensionador.png)

<!---
A seguir apresentam-se os diversos elementos que compõe o projeto do subsistema de estruturas. Estes elementos foram divididos em elemento de estrutura, responsável pela forma do produto, e elemento mecânico, responsável pela movimentação do produto, na concepção da arquitetura do projeto, porém ambos os elementos estão sendo desenvolvidos de forma integrada desde sua concepção, sendo assim de difícil separação. Logo, para apresentação da vista explodida e das renderizações, esta seção trata de ambos de forma simultânea e integrada.

Abaixo estão apresentadas as renderizações do sistema estrutural.

**Figura 11: Vista 3D do subsistema estrutural**

![estrutura](assets/images/3d1.jpg)

![estrutura](assets/images/3d2.jpg)

Abaixo está apresentada a vista explodida do sistema estrutura, bem como a lista de componente da solução com a escolha de material para cada peça.

**Figura 12: Vista explodida do subsistema estrutural**

![estrutura](assets/images/vista_explodida_drawing.png)

**Tabela 10: Lista de componentes**

| Número | Peça                     | Descrição          | Quantidade | Material     |
|:------:|--------------------------|--------------------|:----------:|--------------|
|    1   | Placa de aço superior    | Apêndice 2, desenho 1            |      1     | Aço          |
|    2   | Perfil estrutural v-slot | 20mm x 20mm x 2m   |      5     | Alumínio     |
|    3   | Tubo de PVC              | 20mm x 1.5mm x 2m  |      2     | PVC soldável |
|    4   | Eixo GT2                 | 60 dentes          |      1     | Alumínio     |
|    5   | Eixo GT2                 | 20 dentes          |      3     | Alumínio     |
|    6   | Motor de passo           | NEMA 23            |      2     |              |
|    7   | Perfil estrutural v-slot | 20mm x 20mm x 45mm |      2     | Alumínio     |
|    8   | Cantoneira               |                    |     12     | Alumínio     |
|    9   | Roda de borracha         | 4 polegadas        |      8     | Aço/Borracha |
|   10   | Placa de aço meio        | Apêndice 2, desenho 2           |      1     | Aço          |
|   11   | Placa de aço inferior    | Apêndice 2, desenho 3           |      1     | Aço          |
|   12   | Tubo de PVC para água    | 20mm x 1.5mm x 2m  |      1     | PVC Soldável |
|   13   | Cap PVC                  | 20mm x 1.5mm       |      1     | PVC Soldável |
|   14   | Eixo rodas sup. e inf.   | Apêndice 2, desenho 4           |      2     | Impressão 3D |
|   15   | Inserto metálico         | M3                 |      4     | Latão        |
|   16   | Eixo rodas laterais      | Apêndice 2, desenho 5          |      2     | Impressão 3D |
|   17   | Apoio eixo roda meio     | Apêndice 2, desenho 6            |      1     | Impressão 3D |
|   18   | Caixa motor NEMA 23      | Apêndice 2, desenho 7            |      2     | Impressão 3D |
|   19   | Correia GT2              | 400mm fechada      |      1     | Borracha     |
|   20   | Correia GT2              | 852mm fechada      |      1     | Borracha     |
|   21   | Perfil estrutural v-slot | Corte para correia |      1     | Alumínio     |
|   22   | Eixo roda meio           | Apêndice 2, desenho 8            |      2     | Aço          |
|   23   | Pano de microfibra sup.  | 950mm comprimento  |      2     | Microfibra   |
|   24   | Pano de microfibra inf.  | 900mm comprimento  |      2     | Microfibra   |
|   25   | Rolamento                | 20mm x 15mm x 47mm |      6     | Aço          |
|   26   | Mancal rolamento 47mm    | Apêndice 2, desenho 9            |      6     | Impressão 3D |
|   27   | Rolamento                | 12mm x 8mm x 28mm  |      2     | Aço          |
|   28   | Mancal rolamento 28mm    | Apêndice 2, desenho 10           |      2     | Impressão 3D |

## **Projeto da estrutura**

A estrutura, composta pelos componentes listados acima, tem como principais elementos: os perfis estruturais de alumínio v-slot 20mm x 20mm, que são soluções comerciais; e as placas metálicas, que são soluções desenvolvidas pela equipe, a priori manufaturadas a partir de uma chapa de aço ou alumínio de 4mm a 6mm de espessura. Tendo em vista que o limpador não necessita suportar esforços significativos devido à carregamentos externos, a solução comercial de perfis estruturais de alumínio oferece à estrutura a rigidez e a resistência necessárias a fim de manter o protótipo coeso e funcional.

Já as placas metálicas desenvolvidas pela equipe ainda carecem de uma análise minuciosa antes de sua fabricação, seja em relação ao processo de manufatura, a fim de evitar tratamentos térmicos indesejados pelo processo de corte CNC, ou pela integração com as equipes de eletrônica e energia, a fim de dispor do espaço e condições favoráveis para a operação dos componentes eletrônicos e da bateria. A está em contato com fornecedores buscando uma solução que mais se adeque ao projeto, seja através da escolha do material e/ou através da espessura adequada da chapa metálica. Somente após sanadas todas as dúvidas quanto à manufatura da placa metálica é que a equipe poderá decidir as dimensões finais deste componente e realizar os cálculos estruturais para garantir sua conformidade com o peso máximo do projeto (30 kg) e rigidez necessária.

-->

# **Simulações**

As simulações realizadas têm por objetivo verificar a aplicabilidade das soluções propostas, e permitir a seleção de materiais.

## **Simulação da linha elástica**

Devido a sua longa extensão de 2 metros de comprimento, uma das preocupações principais que se tem com o limpador é garantir que este tenha rigidez o suficiente para evitar deslocamentos excessivos da estrutura devido a seu peso próprio e ao peso dos componentes nela instalados. Para realizar esta verificação, utilizou-se a teoria de linha elástica proposto nos livros de resistência dos materiais (HIBBELER, 2010), regido pela equação abaixo.

[![\\ \mathrm{EI}\frac{d^2v}{dx^2}=\mathrm{M}(x)](https://latex.codecogs.com/svg.latex?%5C%5C%20%5Cmathrm%7BEI%7D%5Cfrac%7Bd%5E2v%7D%7Bdx%5E2%7D%3D%5Cmathrm%7BM%7D(x))](#_)

Onde:
- E é o módulo de elasticidade;
- I é o momento de inércia da viga;
- v é a flecha;
- x é a coordenada a partir de uma das extremidades da viga
- M(x) é o momento gerado pelos carregamentos.

Para tanto um modelo simplificado do limpador solar foi montado. Simplificou-se o limpador para um modelo de viga, conforme apresentado na Fig. XX, engastado em uma de suas extremidades, simulando o lado superior do limpador, que tem rodas em disposição tal maneira que não permitem o deslocamento ou rotação no plano do limpador, e simplesmente apoiado na outra extremidade.

Para montagem do modelo de viga, é necessário atribuir um momento de inércia e módulo de elasticidade à viga. No caso estudado, o momento de inécia é relativo às três barras de alumínio extrudadas dispostas da forma como mostrado nos desenhos técnicos. Este momento de inércia das três barras combinadas foi extraído da ferramenta de propriedades geométricas do SolidWorks, CAD utilizado para a montagem do modelo complexo. Já para o módulo de elasticidade, como a solução escolhida foi a utilização de barras extrudadas em alumínio disponível comercialmente, esta grandeza pode ser obtida a partir dos dados informados pelo fornecedor. Assim, têm-se as propriedades de material e geométricas necessárias para a resolução deste problema, sendo:

- Momento de Inércia: 3,696*10^-6^ m^4^
- Módulo de Elasticidade do Alumínio: 70,3 GPa


Além das propriedades citadas, é necessária também a determinação dos carregamentos aos quais a vigas é submetida, para determinação dos momentos, necessários na equação da linha elástica. O primeiro carregamento a se considerar é o peso próprio do alumínio. Este pode ser obtido dos mesmo dados do fornecedor das barras extrudadas, fornecido tipicamente em unidades de massa por comprimento. Multiplicando este valor pelo número de barras e pela aceleração da gravidade, obtém-se o carregamento distribuído correspondente ao peso próprio das barras de alumínio.

- Massa por comprimento: 0,409 kg.m^-1^
- Número de barras: 3 barras
- Aceleração da gravidade: 9,81 m.s^-2^
- Peso distribuído calculado: 12,04 N.m^-1^ 

Por fim, devem ser consideradas cargas pontuais na estrutura. Uma vez que as barras de alumínio são engastadas às placas de metal, que compõem a estrutura e suportam os sistemas adicionais, estas placas foram desconsideradas no modelo de viga, sendo modeladas apenas como uma massas concentrada em suas respectivas posições. Além disto, todos os dispositivos que são suportados por estas placas (motores, componentes eletrônicos, bateria) também foram modelados como cargas concentradas. Assim, têm se os seguintes pesos distribuídos:

- Parte superior: 98,10 N
- Centro: 49,05 N
- Parte inferior: 73,58 N

Com a definição de todas as grandezas necessárias para a aplicação da teoria de linha elástica, o software FTools foi utilizado para modelagem e solução do modelo.


**Figura 14: Modelo de viga do limpador**

![mod_viga](assets/images/Simulações/Sim1/modelo_vigav2.png)

Ao resolver o modelo proposto, obtém-se que a máxima deflexão do limpador é de 0,018 milímetros para baixo e ocorre em 1,11 metros do engaste. Este deslocamento é significativamente inferior à distância de 1 centímetro da parte inferior do limpador até a superfície da placa solar, indicando que não há necessidade de apoios no centro do limpador.

**Figura 15: Resultados da análise do modelo de viga do limpador**

![result_viga](assets/images/Simulações/Sim1/resultados_viga_em_metros.png)

Apesar dos resultados obtidos indicarem que não há necessidade de um apoio na parte central, este apoio ainda assim será utilizado para garantir o melhor deslocamento do limpador sobre a placa, já que estes pontos de apoio são na verdade rodas.


## **Simulação dos eixos laterais**

Os eixos laterais, peça indicada nos desenhos técnicos de número X, são a peça que garante a fixação do limpador sobre a placa. São os eixos das rodas que ficam apoiadas na lateral superior da placa solar, garantindo que o alinhamento do limpador e garantindo que este ficará fixo à placa em inclinações de 5° a 20°.

Devido a seu papel crítico no funcionamento da estrutura, esta peça deve atender aos seguintes critérios:

- Suportar as tensões devido ao peso do limpador;
- Não sofrer deformações superiores a 1 milímetro;
- Ser fabricada em material que pode ser lubrificado para garantir o bom funcionamento das rodas laterais.

Outra razão pela qual esta peça foi selecionada para simulação é a forma como ela é presa à estrutura. Os outros eixos serão usinados e presos à estrutura com rosca interna ao eixo. Este é preso a estrutura por meio de dois perfis de alumínio, que foram furados para passagem encaixe por fricção do eixo. Portanto julgou-se necessária a simulação do conjunto completo para garantir que as tensões no contato entre os perfis de alumínio e o eixo não seria excessiva para o material escolhido.

Para a simulação procedeu-se da seguinte forma:

1. Identificação das forças atuantes e condições de contorno aplicáveis;
2. Simulação da geometria para avaliação da tensão;
3. Seleção de material pelo critério de tensão (critério de falha de von Mises com aplicação fator de segurança);
4. Simulação da geometria com atribuição de material para verificação das deformações.

### **Identificação das condições de contorno**

**Figura 16: Geometria do conjunto do eixo simulado com condições de contorno**

![geometria_eixo](assets/images/Simulações/Sim2/geometria.png)

Para simular o conjunto, foi aplicado às faces dos perfis de alumínio que têm contato com o resto da estrutura, a condição de engaste, ou _fixed support_.

Para o contato entre o eixo e os perfis de alumínio foi utilizada a condição de contato _Rough_, descrita no Manual do ANSYS como a condição que não permite deslocamento tangencial, mas permite deslocamento normal à superfície. É a condição ideal para modelar o caso de uma peça encaixada por fricção e que é apoiado em ambos os lados.

Por fim, para determinar a força que deveria ser aplicada, utilizou-se a componente normal à placa solar da massa da estrutura, considerando a condição onde esta componente é maior, a condição de 20° de inclinação. No plano da máquina, o conjunto simulado é duplicado, mas optou-se por considerar, simulação, que apenas uma roda suporta a massa completa da estrutura, efetivamente aumentando o coeficiente de segurança que será adotado na seleção do material por dois. Assim, considerando a massa máxima esperada de 25 kg, o peso que deve ser aplicado é de 83,88 N.

### **Avaliação da tensão e seleção de material**

Pra validar a simulação, a geometria foi simulada com diversas malhas, cada vez mais fina. A simulação foi considerada validada com a convergência do valor da tensão, com diferença inferior a 1%. O gráfico abaixo apresenta esta convergência.

**Figura 17: Gráfico de convergência da tensão**

![graph_eixo](assets/images/Simulações/Sim2/tensão_eixo.png)

Assim, tem-se que a tensão no eixo é de 3,40 MPa, e considerar-se-á a malha mais refinada para a simulação da deformação. A figura abaixo apresenta. para clareza da leitura, apenas o eixo e a distribuição das tensões neste.

**Figura 18: Resultados da tensão no eixo**

![tensão_eixo](assets/images/Simulações/Sim2/tensão.png)

A tensão apresentada é a tensão equivalente de von Mises. Essa tensão é utilizada como critério de falha, já que se trata das tensões principais atuantes na peça. Assim, esta tensão pode ser comparada às tensões de escoamento, para materiais dúcteis, ou de ruptura, para materiais frágeis, permitindo assim a seleção de um material apropriado. A tabela abaixo trás as tensões de escoamento para e os materiais que foram avaliados para utilização na fabricação destes eixos, bem como o fator de segurança atrelado a cada um.

**Tabela 10: Avaliação de possíveis materiais**

| Material | Tensão de escoamento | Fator de Segurança |
| ------ | ------ | --- |
| Aço | 195 MPa | 57,4 |
| Alumínio |   110 MPa     | 32,4 |
| Náilon |   60 MPa     | 17,5 |

Por ter uma valor de coeficiente de segurança muito alto, a peça seria superdimensionada se fosse fabricada em alumínio ou aço. Já sendo fabricada em náilon, ainda tem-se um coeficiente de segurança suficientemente elevado, com a vantagem de se tratar de um material auto-lubrificante. O náilon atende aos critérios estabelecidos, além de ser um material relativamente barato, de fácil usinagem e produzir uma peça mais leve que se fabricada em metal. Para validar esta escolha basta avaliar se o critério de deformação máxima é atendido.



### **Avaliação da deformação**

Para avaliação deste critério a malha já validada anteriormente foi utilizada. Todos os parâmetros da simulação, exceto pela atribuição no náilon como material ao eixo, foram mantidos inalterados. A imagem abaixo apresenta os resultados de deformação total.

**Figura 19: Deformação do eixo**

![deformaçao_eixo](assets/images/Simulações/Sim2/deformaçao.png)

Como a máxima deformação foi de 0,07 milímetros, inferior aos critérios estabelecidos, pode-se considerar a peça em náilon como validada.



## **Simulação dos suportes de eixo**

O suporte de eixos é a peça ilustrada nos desenhos técnicos de número X. Ela é responsável por suportar os eixos das rodas que ficam sobre a placa. Para servir de suporte a estes eixos esta peça deve encaixar e poder ser fixa ao perfil de alumínio inferior.


Para esta simulação um métodos análogo à simulação anterior foi adotado. A diferença foi apenas a avaliação de diferentes geometrias possíveis. Cada uma foi avaliada segundo o critério de tensão, para seleção da geometria e do material, para então refazer a análise para verificação do critério de deformação com o material escolhido.

As três geometrias avaliadas foram:
- Peça com cantos vivos;
- Peça com cantos arredondados;
- Peça com cantos arredondados e entradas para facilitar a usinagem.

**Figura 20: Geometrias de suportes explorada**

![G1](assets/images/Simulações/Sim3/G1.png)
![G2](assets/images/Simulações/Sim3/G2.png)
![G3](assets/images/Simulações/Sim3/G3.png)

### **Identificação das condições de contorno**

Para simular as condições de funcionamento do suporte, a face que fica apoiada no perfil de alumínio foi considerada como engastada. A força equivalente a dois terço da massa do limpador foi dividida e aplicada nos furo destinado ao encaixe dos eixos da roda. Sendo assim 80 MPa foram aplicados a cada furo.

**Figura 21: Condições de contorno aplicadas na geometria**

![CC](assets/images/Simulações/Sim3/cond_contorno.png)

### **Avaliação da tensão e seleção de material**

Pra validar a simulação, as três geometrias forma simuladas com diversas malhas, cada vez mais fina. A simulação foi considerada validada com a convergência do valor da tensão.

O valor da tensão máxima obtido em cada geometria foi:

- 7,53 MPa na peça com cantos vivos;
- 7,39 MPa Peça com cantos arredondados;
- 14,69 na peça com cantos arredondados e entradas para facilitar a usinagem.

A geometria com as entradas para facilitar a usinagem da peça apresenta tensão duas vezes superior às outras geometrias, que têm tensão muito próximas. Assim, escolheu-se prosseguir com a geometria com cantos arredondados. A figura abaixo apresenta a distribuição das tensões na peça selecionada.

**Figura 22: Tensões no suporte**

![tensoes_apoio](assets/images/Simulações/Sim3/tens_arr.png)

A tensão apresentada é a tensão equivalente de von Mises.

Para a seleção do material desta peça procedeu-se de forma diferente. A fabricação a peça com a geometria propostas com materiais metálicos seria inviável do ponto de vista econômico, tendo em vista o orçamento do grupo e produziria peça muito pesadas. O material encontrado que era vendido segundo a necessidade do projeto, em chapas de 2 centímetros de espessura, foi o polietileno de alta densidade, PEAD. Este material apresenta boa resistência, alto módulo de elasticidade, para um polímero, baixa densidade, e boa resistência à degradação por calor ou raios UV, que são uma preocupação para um limpador de placas solares que poderá ficar exposto ao sol.

O PEAD apresenta tensão de ruptura de 28 MPa, inferior à máxima tensão à qual a peça será submetida. Sendo assim, pode-se utilizar este material, tendo-se um coeficiente de segurança de 4. Basta avaliar se ele atende o critério de máxima deformação.


### **Avaliação da deformação**

Para avaliação deste critério a malha já validada anteriormente foi utilizada. Todos os parâmetros da simulação, exceto pela atribuição no PEAD como material da peça, foram mantidos inalterados. A imagem abaixo apresenta os resultados de deformação total.


**Figura 23: Deformações do suporte**

![deff_apoio](assets/images/Simulações/Sim3/deff-arr.png)

Como a máxima deformação foi de 0,7 milímetros, inferior aos critérios estabelecidos, pode-se considerar a peça em PAED como validada.
