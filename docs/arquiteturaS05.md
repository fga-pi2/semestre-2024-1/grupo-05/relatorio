# **Arquitetura do subsistema de limpeza**

Uma fonte externa de água pressurizada alimenta uma tubulação perfurada que injeta água na superfície da placa para facilitar a limpeza. O controle da vazão é realizado por uma válvula solenoide.

**Figura 8: Arquitetura do subsistema de limpeza**

![arquitetura_hidr](./assets/images/arq-Hidraulica.png)

