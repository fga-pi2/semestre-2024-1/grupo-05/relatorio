# **Apêndice 07 - Memorial de decisões de desenvolvimento de software**

Registra-se as principais decisões realizadas durante a realização do projeto, particularmente as relativas à escolha de tecnologias para o desenvolvimento do software, arquiteturas do software, inovações ou arquitetura da informação.

## Decisões

21.04.2024 \--\> (escolha de tecnologia) Escolheu-se o uso de Arduino IDE para programação da ESP, já que imaginava-se que a sua facilidade de uso seria ideal para o projeto, além disso, o protocolo MQTT seria suficiente para o andamento do projeto e comunicação com o aplicativo.

21.04.2024 \--\> (escolha de tecnologia) Escolheu-se o uso de Flutter e SQLite como escolha das tecnologias responsáveis pelo aplicativo, a partir do conhecimento dos integrantes da equipe nas tecnologias e por saber que a curva de aprendizado não era tão acentuada. Além disso, sabia-se que o Flutter tem bibliotecas para comunicação com MQTT (necessário com a ESP).

01.06.2024 \--\> (mudança de tecnologia escolhida) O uso do Arduino IDE foi alterado para o framework ESP-IDF, uma vez que a equipe de desenvolvimento já havia experiência no ESP-IDF e, portanto, apesar de antes pensar que o uso do Arduino IDE poderia atender o projeto, acabou por se tornar um gargalo seu aprendizado.