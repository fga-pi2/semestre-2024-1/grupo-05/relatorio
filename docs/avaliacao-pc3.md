# Autoavaliação Ponto de Controle 3

**Tabela 27: Autoavaliação dos integrantes do grupo para o PC3**

| Integrante | Matrícula | Contribuições | Nota |
| :-------: |:---:| :-------------: | :---: |
| Victor Hugo Oliveira Leão | 200028367 | Nessa parte trabalhei na integração entre as áreas, sendo uma ponte entre as outras engenharias e os desenvolvedores, para entregar as funcionalidades de forma integral. | 10 |
| Vinicius Assumpcao de Araújo  | 200028472 | Atuei como parte do time de desenvolvimento, no no desenvolvimento de alguns fluxos de exceção no app, aplicacao de feedbacks e correcao de equivocos e suporte geral na integracao total do projeto. | 10 |
| João Pedro de Camargo Vaz  | 200020650 | Atuei como parte do time de desenvolvimento, no no desenvolvimento de alguns fluxos de exceção no app, aplicacao de feedbacks e correcao de equivocos e suporte geral na integracao total do projeto. | 10 |
|Gabriel Barbosa Pfeislticker de Knegt| 180101056 | -|10|
|Daniel Alberto dos Santos Filho|180030990|Atuei no desenvolvimento das soluções mecânicas, avaliação de requisitos e integração.|10|
|Matheus Matos Fernandes|170111156|Atuei na elaboração do plano de integração, na instalação dos fios e botões e nos testes de integração e validação|10|
|Cícero Barrozo Fernandes Filho|190085819|Atuei no desenvolvimento de alguns fluxos de exceção na esp, como a tratativa de enviar de volta a notificação para o app no término de alguma limpeza. Também estive presente com o time em todas as reuniões de integração e de testes do sistema.|10|
|Christian Fleury Alencar Siqueira|190011602|Atuei no desenvolvimento de fluxo de execção de falha de conexão com a ESP, implementação de handshake para as mensagens enviadas à ESP, mostrando avisos de problemas de conexão e impedindo o usuário de iniciar ou parar limpeza. Melhorias na interface da página de limpador. Melhorias relacionadas ao ID de limpador e ao contador de limpeza. |10|
|Bernardo Chaves Pissutti|190103302|Atuei no desenvolvimento do aplicativo corrigindo bugs no tratamento dos inputs dos usuários e limpando o código.|10|
|Thiago Siqueira Gomes|190055294|-|10|
|Matheus Soares Arruda|190093480|Atuei no desenvolvimento das condições de contorno referente ao motor e sensores no software embarcado|10|
|Guilherme Sanchez Dutra|180113500|Atuei na elaboração do plano de integração, na instalação dos fios e botões e nos testes de integração e validação, além de correção documental|10|
| Gabriel Roger Amorim da Cruz  | 200018248 | Atuei como parte do time de desenvolvimento, contribuindo no desenvolvimento de alguns fluxos de exceção no app, aplicação de feedbacks e correção de equívocos e suporte geral na integração total do projeto. | 10 |
| João Pedro de Camargo Vaz | 200020650 | - | 10 |
|Patrick Christian de Melo|180036432|-|10
|Lucas Pinheiro de Souza|160156866|-|10
