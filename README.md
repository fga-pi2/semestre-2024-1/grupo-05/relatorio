# Projeto Integrador 2 - Grupo 4 (Limpador de Placas Solares)

Relatório: https://lappis-unb.gitlab.io/fga-pi2/semestre-2024-1/grupo-05/relatorio/

## Configuração

Execute `make docs` para acessar o servidor. Para construir os estáticos, execute `make build-docs`.


## Adição de novos documentos

1. Adicionar novo documento na pasta `./docs`
2. Adicionar o nome do arquivo no `mkdocs.yml`

```
nav:
  - Home: 'index.md'
  - 'Semestres Anteriores': 'anteriores.md'
```

3. Testar localmente
