FROM python:3.10
RUN pip install mkdocs mkdocs-material mkdocs-pdf-export-plugin
COPY . /docs
WORKDIR /docs
